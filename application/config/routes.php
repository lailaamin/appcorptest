<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Website';
$route['admin']='Admin';
$route['admin/login']='admin/login';
$route['index']='Website';
$route['logout']='admin/logout';



//home_panel routes
$route['home_panel']='HomePage/index';
$route['home_panel/add'] = 'HomePage/add';
$route['home_panel/edit/(:num)'] = 'HomePage/update/$1';
$route['home_panel/delete/(:num)']='HomePage/delete/$1';
$route['home_panel/drag']='HomePage/changePosition';

//contact_us_panel routes
$route['contact_us_panel']='ContactUs/index';
$route['contact_us_panel/add'] = 'ContactUs/add';
$route['contact_us_panel/edit/(:num)'] = 'ContactUs/update/$1';
$route['contact_us_panel/delete/(:num)']='ContactUs/delete/$1';
$route['contact_us_panel/drag']='ContactUs/changePosition';

//partners_panel routes
$route['partners_panel']='OurPartners/index';
$route['partners_panel/add']='OurPartners/add';
$route['partners_panel/edit/(:num)']='OurPartners/update/$1';
$route['partners_panel/delete/(:num)']='OurPartners/delete/$1';
$route['partners_panel/drag']='OurPartners/changePosition';

//our_job_panel routes
$route['our_job_panel']='OurJob/index';
$route['our_job_panel/add']='OurJob/add';
$route['our_job_panel/edit/(:num)']='OurJob/update/$1';
$route['our_job_panel/delete/(:num)']='OurJob/delete/$1';
$route['our_job_panel/drag']='OurJob/changePosition';

//our_services_panel routes
$route['our_services_panel']='OurServices/index';
$route['our_services_panel/add']='OurServices/add';
$route['our_services_panel/edit/(:num)']='OurServices/update/$1';
$route['our_services_panel/delete/(:num)']='OurServices/delete/$1';
$route['our_services_panel/drag']='OurServices/changePosition';

//social_networks_panel routes
$route['social_networks_panel']='SocialNetworks/index';

//about_us_panel routes
$route['about_us_panel']='AboutUs/index';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
