<?php 


//File appcorp/application/models/AdminModel.php:

/**
 * AdminModel class
 *
 * This class implements addData, getData,
 * getDataByID, delete, update, loginUser,
 * updateOrder, getPosition, getIcons
 * methods that manage data between
 * system and application.
 *
 * @package     appcorp/application
 * @subpackage  models
 * @since       23/9/2018
 * @author      Laila Amin <lamin@app-corp.com>
 * @copyright   Copyright (c) 2018, Appcorp.
 * @link        http://localhost/appcorp/
 */


class AdminModel extends CI_Model{

 /**
  * addData method
  *
  * This method is insert data into appcorp database
  * tables which is entered by user in $data array.
  *
  * @access public
  * @param String $table table name that stroing inserted data
  * @param String array $data  of data to be inserted in appcorp databse
  * @return void
  */
  
  public function addData($table,$data)
  {
     $this->db->insert($table,$data);
  }

 /**
  * getData method
  *
  * This method is retrieve data from appcorp database.
  *
  * @access public
  * @param String $table  table name that stroing inserted data.
  * @return array of objects
  */
  
  public function getData($table)
  {
     $q=$this->db->get($table);
	 return $q->result();
  }

 /**
  * getData method
  *
  * This method is retrieve only row of data from appcorp database
  * by using row id.
  *
  * @access public
  * @param integer $id id of the data to be retrived
  * @param String $table table name to retrieve data from
  * @return object
  */

  public function getDataByID($id,$table)
  {
     $this->db->where('ID',$id);
	 $q=$this->db->get($table);
	 return $q->row();
  }

 /**
  * delete method
  *
  * This method is delete row of data from appcorp database
  * by using row id.
  *
  * @access public
  * @param String $table table name to delete data from
  * @param integer $ID  id of the data to be deleted
  * @return void
  */

  public function delete($table,$ID)
  {
 	 $this->db->where('ID',$ID);
	 $this->db->delete($table);
  }

 /**
  * update method
  *
  * This method is update row of data from appcorp database
  * by using row id and array of data to be updated.
  *
  * @access public
  * @param String $table table name for the data to be updated
  * @param integer $id id of the data to be updated
  * @return void
  */

  public function update($table,$id,$data)
  {
  	 $this->db->where('ID',$id);
	 $this->db->update($table, $data);
		
  }

 /**
  * getData method
  *
  * This method is retrieve only row of data from user table
  * by using username and password.
  *
  * @access public
  * @param String $username of user account
  * @param String $password of user account
  * @return object
  */

  public function loginUser($username,$password)
  {
	 $this->db->where('username',$username);
	 $this->db->where('password',$password);
	 $q=$this->db->get('user');
     return $q->row();

  }

 /**
  * updateOrder method
  *
  * This method is update position column from the selected table 
  *
  * @access public
  * @param integer $pid position id to be updated
  * @param String $table table name for the data to be updated
  * @return boolean
  */


  public function updateOrder($pid,$table)
  {
     $c = 1;

    foreach ($pid as $id)
    {
         $this->db->where('ID',$id);
         $this->db->update($table,array('position' => $c));
         $c++;
    }
    return true;
  }

 /**
  * getPosition method
  *
  * This method is retrieve data from the selected table
  * which is ordered by position column in ascending order.  
  *
  * @access public
  * @param integer $pid position id to be updated
  * @param String $table table name for the data to be updated
  * @return array of objects
  */

  public function getPosition($table)
  {
  	 $this->db->order_by("position", "ASC");
	 $query = $this->db->get($table); 
	 return $query->result();

  }

 /**
  * getIcons method
  *
  * This method is retrieve data from our_job and icons tables
  * which is ordered by position column in ascending order.  
  *
  * @access public
  * @return array of objects
  */

  public function getIcons()
  {
     $q = $this->db->query(" SELECT o.ID AS ID, o.title, o.icon, o.description,i.ID AS iconid,i.name FROM our_job o 
	                INNER JOIN icons i ON o.icon = i.ID ORDER BY position ASC");
 	 return $q->result();
  }

}

?>