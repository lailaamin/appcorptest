<body>
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default" style="margin-top: 50pt;">
				<div class="panel-heading">Log in</div>
				<div class="panel-body" >
					<form role="form" action="<?php echo base_url();?>admin/login" method="POST">
						<fieldset>
							<div class="form-group">
								<label>Username</label>
								<input class="form-control" placeholder="Username" name="username" type="text" autofocus="">
							</div>
							<div class="form-group">
								<label>Password</label>
								<input class="form-control" placeholder="Password" name="password" type="password">
							</div>
							<div class="checkbox">
								<label>
									<input name="remember" type="checkbox" value="1">Remember Me
								</label>
							</div>
							<button type="submit" name="login" value="login"  class="btn btn-primary">Login</button></fieldset>
						</form>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->	
		