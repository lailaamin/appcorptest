<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Appcorp - Admin</title>
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/logo.png">

  <link href="<?php echo base_url();?>adminassets/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>adminassets/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>adminassets/css/datepicker3.css" rel="stylesheet">
  <link href="<?php echo base_url();?>adminassets/css/styles.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/fonts/picons.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <?php if ($this->session->userdata('logged_in') === FALSE)
  {
    redirect('admin/login', 'refresh');
  } 
  elseif($this->session->userdata('logged_in') === TRUE && $this->uri->segment(2)!=null && $this->uri->segment(2)==="login")
  {
     redirect('home_panel', 'refresh');
  } 
  ?>
</head>

<body>
  <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse" ><span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span></button>
          <a class="navbar-brand" href="#"><span>Appcorp</span> Admin</a>
      </div>
    </div><!-- /.container-fluid -->
  </nav>