
<div class="row" style="margin: auto; width: 670pt;" >

 <div class="col-lg-12">
  <h1 class="page-header">Partners</h1>
</div>
</div><!--/.row-->
<div class="row" style="margin: auto; width: 650pt;margin-left: 170pt;" >
  <div class="col-lg-12" style="width: 550pt;">
    <div class="panel panel-default">
      <div class="panel-heading">Add Partners</div>
      <div class="panel-body">
        <div class="col-md-12">
          <?php if(isset($result)) {?>
            <form role="form" method="post" id="upload_form1" align="left" enctype="multipart/form-data" action="<?php echo base_url();?>partners_panel/edit/<?php echo $result->ID;?>">
               <div class="form-group">
                <label>Partner Logo</label>
                <input type="file" name="img" id="image_file1">
              </div>
              <br />
              <br />
              <div id="uploaded_image1">
                <img src="<?php echo base_url().'upload/'. $result->logo; ?>" width="300" height="225" class="img-thumbnail" />
              </div>

              <br><br>
              <div class="pull-left" style="margin-left: 316pt;">
                <button type="submit" name="submit1" value="upload" id="upload" class="btn btn-success">Save&nbsp;<span class="glyphicon glyphicon-new-window"> </span></button>
              </div>
            </form>
            <?php } else {?>

            <form role="form" method="post" id="upload_form" action="<?php echo base_url();?>partners_panel/add" align="left" enctype="multipart/form-data">
             <div class="form-group">
              <label>Partner Logo</label>
              <input type="file" name="img" id="image_file" >
              </div>
              <br />
              <br />
              <div id="uploaded_image">
             </div>

             <br><br>
             <div class="pull-left" style="margin-left: 318pt;">
              <button type="submit" name="submit1" value="upload" id="upload" class="btn btn-success">Add&nbsp;<span class="glyphicon glyphicon-new-window"> </span></button>
             </div>
           </form>
          <?php }?>
          <form role="form" method="post" action="<?php echo base_url();?>partners_panel/add" >
           <div class="pull-right">
              <button type="reset"  name="submit1" class="btn btn-primary">Reset</button>
              <button type="submit" name="submit1" value="cancel" class="btn btn-default">Cancel</button>
            </div>
          </form>
        </div>
       </div>
     </div>
   </div><!-- /.panel-->
  </div>
</div><!-- /.row-->

<script>
  $(document).ready(function(){
    $('#upload_form1').on('submit', function(e){
      e.preventDefault();
      if($('#image_file1').val() == '')
      {
        alert("Please Select the image");
      }
      else
      {
        $.ajax({
          url:"<?php echo base_url(); ?>partners_panel/edit/<?=$logoId;?>", 
                //base_url() = http://localhost/tutorial/codeigniter
                method:"POST",
                data:new FormData(this),
                contentType: false,
                cache: false,
                processData:false,
                success:function(data)
                {$('#uploaded_image1').html(data);

              }
            });
      }
    });
  });
</script>
