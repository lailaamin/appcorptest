<div class="row" style="margin: auto; width: 670pt;" >
  <div class="col-lg-12">
    <h1 class="page-header">About Us</h1>
  </div>
</div><!--/.row-->
<div class="row" style="margin: auto; width: 650pt;margin-left: 170pt;" >
 <div class="col-lg-12" style="width: 550pt;">
  <div class="panel panel-default">
    <div class="panel-heading">Add information to the website</div>
    <div class="panel-body">
      <div class="col-md-12">
        <form role="form" method="POST" action="<?php echo base_url();?>about_us_panel">
          <?php $obj = $result[0]; ?>
          <div class="form-group">
            <label>Title</label>
            <input class="form-control" name="title" value="<?= $obj->title;?>" placeholder="Enter title">
          </div>

          <div class="form-group">
            <label>Body</label>
            <textarea class="form-control" name="body"  rows="7" placeholder="Enter Body"><?= $obj->body;?></textarea>
          </div>

          <div class="pull-right">
            <button type="submit" name="submit1" value="save" class="btn btn-success">Save&nbsp;<span class="glyphicon glyphicon-new-window"> </span></button>
            <button type="reset"  name="submit1"  class="btn btn-primary">Reset</button>
          </div>
        </form>
      </div>
    </div>
  </div><!-- /.panel-->
</div>
</div><!-- /.row-->