<div class="row" style="margin: auto; width: 670pt;" >
  <div class="col-lg-12">
    <h1 class="page-header">Social Networks</h1>
  </div>
</div><!--/.row-->
<div class="row" style="margin: auto; width: 650pt;margin-left: 170pt;" >
  <div class="col-lg-12" style="width: 450pt;">
    <div class="panel panel-default">
      <div class="panel-heading">Social Links</div>
      <div class="panel-body">
        <div class="col-md-12">
          <form role="form" method="POST" action="<?php echo base_url();?>social_networks_panel">
            <?php $obj = $result[0]; ?>
            <div class="form-group">
              <label>Facebook</label>
              <input class="form-control" name="Facebook" value="<?php echo $obj->Facebook;?>" placeholder="Enter Facebook URL">
            </div>

            <div class="form-group">
              <label>Linkedin</label>
              <input class="form-control" name="Linkedin" value="<?php echo $obj->Linkedin;?>" placeholder="Enter Linkedin URL">
            </div>

            <div class="form-group">
              <label>Twitter</label>
              <input class="form-control" name="Twitter" value="<?php echo $obj->Twitter;?>" placeholder="Enter Twitter URL">
            </div>

            <div class="form-group">
              <label>Instagram</label>
              <input class="form-control" name="Instagram" value="<?= $obj->Instagram;?>" placeholder="Enter Instagram URL">
            </div>


            <div class="pull-right">
              <button type="submit" name="submit1" value="save" class="btn btn-success">Save&nbsp;<span class="glyphicon glyphicon-new-window"></button>
                <button type="reset"  name="submit1"  class="btn btn-primary">Reset</button>
            </div>
          </form>
        </div>
      </div>
    </div><!-- /.panel-->
  </div>
</div>