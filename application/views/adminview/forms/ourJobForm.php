<div class="row" style="margin: auto; width: 670pt;">
  <div class="col-lg-12" >
    <h1 class="page-header">What we do</h1>
  </div>
</div><!--/.row-->
<div class="row" style="margin: auto; width: 650pt;" >
  <div class="col-lg-12" style="width: 550pt;">

    <div class="panel panel-default">
      <div class="panel-heading">Add more about our job</div>
      <div class="panel-body">
        <div class="col-md-12">
          <?php if (isset($result1)) { ?>

            <form role="form" method="POST"  action="<?php echo base_url();?>our_job_panel/edit/<?php echo $result1->ID;?>">
              <div class="form-group">
                <label>Title</label>
                <input class="form-control" name="title" placeholder="Enter title" value="<?php echo $result1->title; ?>">
              </div>
              <div class="form-group">
                <label>Description</label>
                <textarea class="form-control ckeditor" rows="7" name="description" placeholder="Enter Description" ><?php echo $result1->description; ?></textarea>
              </div>
              <?php foreach ($result as $obj) {?>
                <div class="col-md-3 text-center">
                  <div class="services-box-3">
                    <i class="<?php echo $obj->name;?> wow fadeInDown animated" data-wow-duration="700ms" data-wow-delay="700ms">
                      <i class="circle-border"></i>
                    </i>
                    <br><br>
                    <input type="radio" id="icons" name="icons" value="<?=$obj->ID; ?>"
                    <?= $obj->ID == $result1->icon?"checked":""; ?> />
                  </div>
                  <br>
                </div>
            <?php } }else {?>
              <form role="form" method="POST" action="<?php echo base_url();?>our_job_panel/add">
                <div class="form-group">
                  <label>Title</label>
                  <input class="form-control" name="title" placeholder="Enter title">
                </div>
                <div class="form-group">
                  <label>Description</label>
                  <textarea class="form-control ckeditor" rows="7" name="description" placeholder="Enter Description"></textarea>
                </div>
                <?php foreach ($result as $obj) {?>
                  <div class="col-md-3 text-center">
                    <div class="services-box-3">
                      <i class="<?php echo $obj->name;?> wow fadeInDown animated" data-wow-duration="700ms" data-wow-delay="700ms">
                        <i class="circle-border"></i>
                      </i>
                      <br><br>
                      <input type="radio" name="icons" value="<?php echo $obj->ID;?>">
                    </div>
                    <br>
                  </div>
              <?php } }?>                   
            </div>
          </div>
          <div style="margin-left: 310pt;">
           <button type="submit" name="submit1" value="save" class="btn btn-success">Save &nbsp;<span class="glyphicon glyphicon-new-window"></button>
            <button type="reset"  name="submit1"  class="btn btn-primary">Reset</button>
            <button type="submit" name="submit1"  value="cancel" class="btn btn-default" >Cancel</button>
            <br><br>
          </form>
          </div>
        </div><!-- /.panel-->
      </div>
    </div><!-- /.row-->