<div class="row" style="margin: auto; width: 670pt;">
  <div class="col-lg-12" >
    <h1 class="page-header">Our Services</h1>
  </div>
</div><!--/.row-->
<div class="row" style="margin: auto; width: 650pt;margin-left: 170pt;" >
  <div class="col-lg-12" style="width: 550pt;">
    <div class="panel panel-default">
      <div class="panel-heading">Add new services</div>
      <div class="panel-body">
        <div class="col-md-12">
          <?php if (isset($result)) {?>

            <form role="form" method="post" id="upload_logo1" align="left" enctype="multipart/form-data"  action="<?php echo base_url();?>our_services_panel/edit/<?php echo $result->ID;?>">
              <div class="form-group">
                <label>Title</label>
                <input class="form-control" name="title" placeholder="Enter title" value="<?php echo $result->title;?>">
               </div>
               <div class="form-group">
                <label>Body</label>
                <textarea class="form-control ckeditor" name="body" rows="7" placeholder="Enter Body"><?php echo $result->body;?></textarea>
               </div>

               <div class="form-group">
                <label>Add Logo</label>
                <input type="file" name="img" id="image_logo1">
               </div>
               <br />
               <br />
               <div id="uploaded_logo1">
                <img name="img" src="<?php echo base_url().'upload/'. $result->logo; ?>" width="300" height="225" class="img-thumbnail" />
               </div>
               <div class="form-group">
                 <label>Choose the logo alignment that will apear in the website </label>
               </div>
               <div class="form-group">
                <?php if($result->align==1){?>
                  <input type="radio" name="align" value="0" >&nbsp;Left&nbsp;   
                  <input type="radio" name="align" value="1" checked> Right&nbsp;
                <?php } else{?>
                  <input type="radio" name="align" value="0" checked>&nbsp;Left&nbsp;   
                  <input type="radio" name="align" value="1" > Right&nbsp;
                <?php }?>
              </div>
              <br><br>
              <div class="pull-left" style="margin-left: 313pt;">
                <button type="submit" name="submit1" value="save" id="upload" class="btn btn-success">Save &nbsp;<span class="glyphicon glyphicon-new-window"> </span></button>
              </div>
            </form>
            <?php } else{?>
            <form role="form" method="post" id="upload_logo" align="left" enctype="multipart/form-data" action="<?php echo base_url();?>our_services_panel/add">
              <div class="form-group">
                <label>Title</label>
                <input class="form-control" name="title" placeholder="Enter title">
              </div>
              <div class="form-group">
                <label>Body</label>
                <textarea class="form-control ckeditor" id="services_body" name="body" rows="7" placeholder="Enter Body"></textarea>

              </div>

              <div class="form-group">
                <label>Add Logo</label>
                <input type="file" name="img" id="image_logo">
              </div>

              <br />
              <br />
              <div id="uploaded_logo">

              </div>
              <div class="form-group">
                <label>Choose the logo alignment that will apear in the website </label>
              </div>
              <div class="form-group">
                <input type="radio" name="align" value="0" checked>&nbsp;Left&nbsp;   
                <input type="radio" name="align" value="1" > Right&nbsp;  
              </div>
              <br><br>
              <div class="pull-left" style="margin-left: 316pt;">
                <button type="submit" name="submit1" value="add" id="upload" class="btn btn-success">Add &nbsp;<span class="glyphicon glyphicon-new-window"> </span></button>
              </div>
            </form>
            <?php }?>
            <form role="form" method="post" action="<?php echo base_url();?>our_services_panel/add">
             <div class="pull-right" >
              <button type="reset"  name="submit1" class="btn btn-primary">Reset</button>
              <button type="submit" name="submit1" value="cancel" class="btn btn-default">Cancel</button>
             </div>
            </form>
           </div>
      </div>
    </div><!-- /.panel-->
  </div>
</div><!-- /.row-->

<script>
  $(document).ready(function(){
    $('#upload_logo1').on('submit', function(e){

      e.preventDefault();
      var data = new FormData(this);
      var body = CKEDITOR.instances.services_body.getData();
      data.append('body',body);
      $.ajax({
        url:"<?php echo base_url(); ?>our_services_panel/edit/<?=$logoId;?>", 
                //base_url() = http://localhost/tutorial/codeigniter
                method:"POST",
                data:data,
                contentType: false,
                cache: false,
                processData:false,
                success:function(data)
                {
                  $('#uploaded_logo1').html(data);
                }
              });
    });
  });

</script>
