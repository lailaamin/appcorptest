<div class="row" style="margin: auto; width: 670pt;" >
 <div class="col-lg-12">
  <h1 class="page-header">Contact Us</h1>
</div>
</div><!--/.row-->
<div class="row" style="margin: auto; width: 650pt;margin-left: 170pt;" >
  <div class="col-lg-12" style="width: 550pt;">
    <div class="panel panel-default">
      <div class="panel-heading">Add contact details</div>
      <div class="panel-body">
        <div class="col-md-12">
         <?php if(isset($result)) {?>
          <form role="form" method="POST" action="<?php echo base_url();?>contact_us_panel/edit/<?php echo $result->ID;?>">

            <div class="form-group">
              <label>Email</label>
              <input class="form-control" name="email" value="<?php echo $result->Email;?>" placeholder="Enter Email">
            </div>
            <div class="form-group">
              <label>Location</label>
              <input class="form-control" name="location" value="<?php echo $result->location;?>" placeholder="Enter location">
            </div>

          <?php }else {?>
            <form role="form" method="POST" action="<?php echo base_url();?>contact_us_panel/add">

              <div class="form-group">
                <label>Email</label>
                <input class="form-control" name="email" placeholder="Enter Email">
              </div>
              <div class="form-group">
                <label>Location</label>
                <input class="form-control" name="location" placeholder="Enter location">
              </div>
            <?php }?>

            <div class="pull-right">
              <button type="submit" name="submit1" value="save" class="btn btn-success">Save&nbsp;<span class="glyphicon glyphicon-new-window"> </span></button>
              <button type="reset"  name="submit1"  class="btn btn-primary">Reset</button>
              <button type="submit" name="submit1" value="cancel" class="btn btn-default">Cancel</button>
            </div>
          </form>
        </div>
      </div>
    </div><!-- /.panel-->
  </div>
</div><!-- /.row