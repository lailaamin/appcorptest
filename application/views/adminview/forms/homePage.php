<div class="row" style="margin: auto; width: 670pt;" >
  <div class="col-lg-12">
    <h1 class="page-header">Home</h1>
  </div>
</div><!--/.row-->
<div class="row" style="margin: auto; width: 650pt;margin-left: 170pt;" >
  <div class="col-lg-12" style="width: 550pt;">
    <div class="panel panel-default">
      <div class="panel-heading">Home</div>
      <div class="panel-body">
        <div class="col-md-12">
         <?php if(isset($result)) {?>
          <form role="form" method="POST" action="<?php echo base_url();?>home_panel/edit/<?php echo $result->ID;?>">

            <div class="form-group">
              <label>Title</label>
              <input class="form-control" name="title" value="<?php echo $result->title;?>" placeholder="Enter title">
            </div>


            <div class="form-group">
              <label>Subtitle/description</label>
              <textarea class="form-control ckeditor" rows="7" name="subtitle" placeholder="Enter Subtitle/Description"><?php echo $result->subtitle;?></textarea>
            </div>

          <?php }else{?>

            <form role="form" method="POST" action="<?php echo base_url();?>home_panel/add">

              <div class="form-group">
                <label>Title</label>
                <input class="form-control" name="title" placeholder="Enter title">
              </div>
              
              <div class="form-group">
                <label>Subtitle/description</label>
                <textarea class="form-control ckeditor" rows="7" name="subtitle" placeholder="Enter Subtitle/Description"></textarea>
              </div>
            <?php }?>

            <div class="pull-right">
              <button type="submit" name="submit1" value="save" class="btn btn-success">Save&nbsp;<span class="glyphicon glyphicon-new-window"> </span></button>
              <button type="reset"  name="submit1"  class="btn btn-primary">Reset</button>
              <button type="submit" name="submit1"  value="cancel" class="btn btn-default">Cancel</button>
            </div>
          </form>
        </div>
      </div>
    </div><!-- /.panel-->
  </div>
</div><!-- /.row-->