<div class="row" style="margin: auto; width: 670pt;" >
	<div class="col-lg-12">
		<h1 class="page-header">Partners</h1>
	</div>
</div><!--/.row-->
 <div class="row" style="margin: auto; width: 650pt;margin-left: 170pt;" >
	<div class="col-lg-12" style="width: 650pt;">
		<div class="panel panel-default">
			<div class="panel-heading">Partners logos</div>
			<br>
			<form class="form" method="POST" action="<?php echo base_url();?>partners_panel">
				<button type="submit" class="btn btn-success" style="margin-left: 22pt;" name="submit" value="add">Add new &nbsp;<span class="glyphicon glyphicon-new-window"> </span></button>
			</form>  
			<div class="panel-body">

				<div class="col-md-12">
					<table class="table table-striped table-bordered table-hover table-condensed" >
						<thead>
							<tr>
								<th>Logo</th>
								<th>Action</th>

							</tr>
						</thead>
						<tbody id="row_position">
							<?php foreach ($result as $obj) {?>
								<tr align="center" id="<?php echo $obj->ID; ?>">
									<td  style="width: 80%"><img class="img-responsive img-thumbnail" src="<?php echo base_url()?>upload/<?php echo $obj->logo; ?>" style="max-width: 35%" alt="not found"></td>
									<td><a class="btn btn-warning" href="<?php echo site_url("partners_panel/edit/{$obj->ID}"); ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                                        &nbsp;
										<a class="btn btn-danger remove3" data-id="<?php echo $obj->ID;?>"><span class="glyphicon glyphicon-trash"></span></a>

									</td>
								</tr>
							<?php }?>
						</tbody>
					</table>
				</div>
			</div>
		</div><!-- /.panel-->
	</div>
</div><!-- /.row-->

