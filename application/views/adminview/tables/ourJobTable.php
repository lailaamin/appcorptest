
<div class="row" style="margin: auto; width: 670pt;" >
	<div class="col-lg-12">
		<h1 class="page-header">what we do?</h1>
	</div>
</div><!--/.row-->
<div class="row" style="margin: auto; width: 650pt;margin-left: 170pt;" >
	<div class="col-lg-12" style="width: 650pt;">
		<div class="panel panel-default">
			<div class="panel-heading">Table of what we do section</div>
			<br>
				<form class="form"method="POST" action="<?php echo base_url();?>our_job_panel">  
					<button type="submit"  class="btn btn-success"  style="margin-left: 22pt;" name="submit" value="add">Add&nbsp;<span class="glyphicon glyphicon-new-window"> </span></button>
				</form>
			<div class="panel-body">
				<div class="col-md-12">
					<table class="table table-striped table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th>Title</th>
								<th>Description</th>
								<th>Logo</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody id="row_position3">
						    <?php foreach ($result as $obj) {?>
							<tr align="center" id="<?php echo $obj->ID; ?>">
								<td><?php echo $obj->title;?></td>
								<td><?php echo $obj->description;?></td>
								<td style="width: 20%"><div class="col-md-3 text-center"><div class="services-box-3" >
									<i class="<?php echo $obj->name;?>"></i></div></div></td>
								<td style="width: 20%" align="center"><a class="btn btn-warning" href="<?php echo site_url("our_job_panel/edit/{$obj->ID}"); ?>"> <span class="glyphicon glyphicon-pencil"></span></a>
									&nbsp;
									<a class="btn btn-danger remove2" data-id="<?php echo $obj->ID;?>"><span class="glyphicon glyphicon-trash"></span></a>
							</tr>
						<?php }?>
					  </tbody>
				</table> 
		      </div>
		   </div>
    	</div><!-- /.panel-->
   </div>
</div>