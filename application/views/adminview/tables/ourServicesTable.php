
<div class="row" style="margin: auto; width: 670pt;" >
	<div class="col-lg-12">
		<h1 class="page-header">Our Services</h1>
	</div>
</div><!--/.row-->
<div class="row" style="margin: auto; width: 650pt;margin-left: 170pt;" >
	<div class="col-lg-12" style="width: 650pt;">
		<div class="panel panel-default">
			<div class="panel-heading">Services Information</div>
			<br>
			<form class="form" method="POST" action="<?php echo base_url();?>our_services_panel">
				<button type="submit" class="btn btn-success"style="margin-left: 22pt;" name="submit" value="add">Add&nbsp;<span class="glyphicon glyphicon-new-window"> </span></button>
			</form>          
			<div class="panel-body">
				<div class="col-md-12">
					<table class="table table-striped table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th>Title</th>
								<th>Service description</th>
								<th>Logo</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody id="row_position4">

							<?php foreach ($result as $obj) {?>
							<tr align="center" id="<?php echo $obj->ID; ?>">
								<td><?php echo $obj->title;?></td>
								<td><?php echo $obj->body;?></td>
								<td><img class="img-responsive img-thumbnail" src="<?php echo base_url()?>upload/<?php echo $obj->logo; ?>"  style="max-width: 35%" alt=""></td>
								<td  style="width: 20%" align="center"><a class="btn btn-warning" href="<?php echo site_url("our_services_panel/edit/{$obj->ID}"); ?>"> <span class="glyphicon glyphicon-pencil"></span></a>&nbsp;
								<a class="btn btn-danger remove4" data-id="<?php echo $obj->ID;?>" ><span class="glyphicon glyphicon-trash"></span></a></td>
							</tr>
						<?php }?>	

					</tbody>
				</table> 
			</div>
		</div>
	  </div><!-- /.panel-->
   </div>
</div><!-- /.row-->