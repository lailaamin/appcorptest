
<div class="row" style="margin: auto; width: 670pt;" >
      <div class="col-lg-12">
        <h1 class="page-header">Social Networks </h1>
      </div>
    </div><!--/.row-->
<div class="row" style="margin: auto; width: 850pt; margin-left: 170pt;" >
      <div class="col-lg-12">

<div class="panel panel-default">
          <div class="panel-heading">Table</div>
          <div class="panel-body">
           <div class="col-md-9">
			  <p>Links Table</p>                                          
			  <table class="table table-striped table-bordered table-hover table-condensed">
			    <thead>
			      <tr>
			      	<th><input type="checkbox" id="checkboxID" name=""></th>
			        <th>Social Network</th>
			        <th>Link</th>
			        <!-- <th>Action</th> -->
			      </tr>
			    </thead>
			    <tbody>
<!-- 			      <tr>
			        <td>Who We Are?</td>
			        <td>Our company is based in Cairo, founded by a team of four experts with more than 15 years of experience in the fields of marketing, telecommunications, business development and technology. Appcorp is a VAS Licensed company under the rules and regulations of National Telecommunication Regulatory Authority (NTRA). Our core competence is the development and management of content, VAS solutions and services. We are dedicated to supporting mobile operators in driving up VAS revenues, by providing them with the most innovative offerings in the industry.</p></td>
			      </tr> -->
			      <?php foreach ($result as $obj) {?>
					<tr> 
						<td><input type="checkbox" name="checkedID[]" value="<?php echo $obj->ID;?>" class="checkbox"/>
						<td><?php echo $obj->title;?></td>
						<td><?php echo $obj->link;}?></td>

					</tr>

			    </tbody>
			 </table> 
			 <form class="form pull-right" method="POST" action="<?php echo base_url();?>admin/socialnetworks">
			  <button type="submit" class="btn btn-primary" name="submit" value="add">Add</button>
               <button type="submit" class="btn btn-default"  name="submit" value="delete">Delete</button>
               <button type="submit" class="btn btn-default " name="submit" value="update">Update</button>
              </form>           
            </div>
          </div>
        </div><!-- /.panel-->
      </div>
    </div>