     <?php $controller = $this->router->fetch_class();
      $method = $this->router->fetch_method();
      $active= $this->uri->uri_string();?>
    <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
      <div class="profile-sidebar">
        <div class="profile-userpic">
          <img src="http://placehold.it/50/30a5ff/fff" class="img-responsive" alt="">
        </div>
        <div class="profile-usertitle">
          <div class="profile-usertitle-name"> <?php echo $this->session->userdata('username');?></div>
          <div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
        </div>
        <div class="clear"></div>
      </div>
      <div class="divider"></div>
      <ul class="nav menu " >
        <li class="<?php if($active=='home_panel'|| $active=='home_panel/add'||$active=='home_panel/edit/'. $this->uri->segment(3)){echo'active';} ?>"><a href="<?php echo base_url();?>home_panel"><i class="fa fa-file-text"></i> Home Panel</a></li>
        <li class="<?php if($active=='about_us_panel'){echo'active';} ?>"><a href="<?php echo base_url();?>about_us_panel" ><i class="fa fa-group"></i> About Us</a></li>
         <li class="<?php if($active=='our_job_panel'|| $active=='our_job_panel/add'||$active=='our_job_panel/edit/'. $this->uri->segment(3)){echo'active';} ?>"><a href="<?php echo base_url();?>our_job_panel"><i class="fa fa-lightbulb-o"></i> Our Job</a></li>   
         <li class="<?php if($active=='our_services_panel'|| $active=='our_services_panel/add'||$active=='our_services_panel/edit/'. $this->uri->segment(3)){echo'active';} ?>"><a href="<?php echo base_url();?>our_services_panel"><i class="fa fa-gears"></i> Our Services</a></li>
        <li class="<?php if($active=='partners_panel'|| $active=='partners_panel/add'||$active=='partners_panel/edit/'. $this->uri->segment(3)){echo'active';} ?>"><a href="<?php echo base_url();?>partners_panel"><i class="fa fa-handshake-o"></i> Partners</a></li>   
        <li class="<?php if($active=='contact_us_panel'|| $active=='contact_us_panel/add'||$active=='contact_us_panel/edit/'. $this->uri->segment(3)){echo'active';} ?>"><a href="<?php echo base_url();?>contact_us_panel"><i class="fa fa-envelope"></i> Contact Us</a></li>
        <li class="<?php if($active=='social_networks_panel'){echo'active';} ?>"><a href="<?php echo base_url();?>social_networks_panel"><i class="fa fa-share-alt"></i> Social Networks</a></li>    
        <li><a href="<?php echo base_url();?>logout"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
      </ul>
    </div><!--/.sidebar-->

   