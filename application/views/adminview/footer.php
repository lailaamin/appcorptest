<script src="<?php echo base_url();?>adminassets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url();?>adminassets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>adminassets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>adminassets/js/custom.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>adminassets/ckeditor/ckeditor.js"></script>\
<script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
<script type="text/javascript">
        //Initiat WOW JS
        new WOW().init();
        function closeNav(){
            $(".navbar-collapse").removeClass("in");
            $(".navbar-collapse").removeClass("show");
        }
    </script>

<!-- <script type="text/javascript" src="/assets/js/partnerJs.js"></script> -->
<script>
    $(document).ready(function(){
       $('#upload_form').on('submit', function(e){

          e.preventDefault();
          if($('#image_file').val() == '')
          {
             alert("Please Select the image");
         }
         else
         {
             $.ajax({
                url:"<?php echo base_url(); ?>partners_panel/add", 
		//base_url() = http://localhost/tutorial/codeigniter
		method:"POST",
		data:new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success:function(data)
		{
            $('#uploaded_image').html(data);
            // alert("Image uploaded successfully");
            document.location.reload();

        }
    });
         }
     });
   });
</script>


<script>
$(document).ready(function(){
    $('#upload_logo').on('submit', function(e){
        var data = new FormData(this);
        var body = CKEDITOR.instances.services_body.getData();
        data.append('body',body);
        e.preventDefault();

        $.ajax({
            url:"<?php echo base_url(); ?>our_services_panel/add", 
        //base_url() = http://localhost/tutorial/codeigniter
        method:"POST",
        data: data,
        contentType: false,
        cache: false,
        processData:false,
        success:function(data)
        {
            $('#uploaded_logo').html(data);
             // alert("Image uploaded successfully")
             document.location.reload();


         }
     });
    });
});
</script>


<script>
 $(document).ready(function(){
   $( "#row_position" ).sortable({
    delay: 150,
    stop: function() {
        var selectedData = new Array();
        $('#row_position > tr').each(function() {
            selectedData.push($(this).attr("id"));
        });
        updatePosition(selectedData);
    }
});
});

 function updatePosition(data) {
    $.ajax({
        url:"<?php echo base_url(); ?>partners_panel/drag",
        type:'post',
        data:{position:data},
        success:function(){
            alert('your change successfully saved');
        }
    })
}
</script>

<script>
 $(document).ready(function(){
   $( "#row_position1" ).sortable({
    delay: 150,
    stop: function() {
        var selectedData = new Array();
        $('#row_position1 > tr').each(function() {
            selectedData.push($(this).attr("id"));
        });
        updatePosition1(selectedData);
    }
});
});

 function updatePosition1(data) {
    $.ajax({
        url:"<?php echo base_url(); ?>home_panel/drag",
        type:'post',
        data:{position1:data},
        success:function(){
            alert('your change successfully saved');
        }
    })
}
</script>


<script>
 $(document).ready(function(){
    $( "#row_position2" ).sortable({
        delay: 150,
        stop: function() {
            var selectedData = new Array();
            $('#row_position2 > tr').each(function() {
                selectedData.push($(this).attr("id"));
            });
            updatePosition2(selectedData);
        }
    });
});

 function updatePosition2(data) {
    $.ajax({
        url:"<?php echo base_url(); ?>contact_us_panel/drag",
        type:'post',
        data:{position2:data},
        success:function(){
            alert('your change successfully saved');
        }
    })
}
</script>

<script>
 $(document).ready(function(){
    $( "#row_position3" ).sortable({
        delay: 150,
        stop: function() {
            var selectedData = new Array();
            $('#row_position3 > tr').each(function() {
                selectedData.push($(this).attr("id"));
            });
            updatePosition3(selectedData);
        }
    });
});

 function updatePosition3(data) {
    $.ajax({
        url:"<?php echo base_url(); ?>our_job_panel/drag",
        type:'post',
        data:{position3:data},
        success:function(){
            alert('your change successfully saved');
        }
    })
}
</script>
<script>
 $(document).ready(function(){
    $( "#row_position4" ).sortable({
        delay: 150,
        stop: function() {
            var selectedData = new Array();
            $('#row_position4 > tr').each(function() {
                selectedData.push($(this).attr("id"));
            });
            updatePosition4(selectedData);
        }
    });
});

 function updatePosition4(data) {
    $.ajax({
        url:"<?php echo base_url(); ?>our_services_panel/drag",
        type:'post',
        data:{position4:data},
        success:function(){
            alert('your change successfully saved');
        }
    })
}
</script>
<script type="text/javascript">
    $(".remove").click(function(){
        var id = $(this).parents("tr").attr("id");


        if(confirm('Are you sure to remove this record ?'))
        {
            $.ajax({
             url: '<?php echo base_url(); ?>contact_us_panel/delete/'+id,
             type: 'DELETE',
             error: function() {
              alert('Something is wrong');
          },
          success: function(data) {
            $("#"+id).remove();
            alert("Record removed successfully");  
        }
    });
        }
    });


</script>

<script type="text/javascript">
    $(".remove1").click(function(){
        var id = $(this).parents("tr").attr("id");


        if(confirm('Are you sure to remove this record ?'))
        {
            $.ajax({
             url: '<?php echo base_url(); ?>home_panel/delete/'+id,
             type: 'DELETE',
             error: function() {
              alert('Something is wrong');
          },
          success: function(data) {
            $("#"+id).remove();
            alert("Record removed successfully");  
        }
    });
        }
    });


</script>

<script type="text/javascript">
    $(".remove2").click(function(){
        var id = $(this).parents("tr").attr("id");


        if(confirm('Are you sure to remove this record ?'))
        {
            $.ajax({
             url: '<?php echo base_url(); ?>our_job_panel/delete/'+id,
             type: 'DELETE',
             error: function() {
              alert('Something is wrong');
          },
          success: function(data) {
            $("#"+id).remove();
            alert("Record removed successfully");  
        }
    });
        }
    });


</script>

<script type="text/javascript">
    $(".remove3").click(function(){
        var id = $(this).parents("tr").attr("id");


        if(confirm('Are you sure to remove this record ?'))
        {
            $.ajax({
             url: '<?php echo base_url(); ?>partners_panel/delete/'+id,
             type: 'DELETE',
             error: function() {
              alert('Something is wrong');
          },
          success: function(data) {
            $("#"+id).remove();
            alert("Record removed successfully");  
        }
    });
        }
    });


</script>
<script type="text/javascript">
    $(".remove4").click(function(){
        var id = $(this).parents("tr").attr("id");


        if(confirm('Are you sure to remove this record ?'))
        {
            $.ajax({
             url: '<?php echo base_url(); ?>our_services_panel/delete/'+id,
             type: 'DELETE',
             error: function() {
              alert('Something is wrong');
          },
          success: function(data) {
            $("#"+id).remove();
            alert("Record removed successfully");  
        }
    });
        }
    });


</script>

<div class="footer1">
  <div class="col-md-12 t-pad10 copyright text-center">
   <p class="bold">© 2018 Appcorp. All rights reserved.</p>
</div>
</div>
</body>
</html>