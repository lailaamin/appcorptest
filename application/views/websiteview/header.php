<!DOCTYPE html>

<html lang="en">

<head>
    <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <title>AppCorp</title>
    <!-- Favicon
        ============================================== -->
        <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/logo.png">
    <!-- Mobile Specific
        ================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- CSS Files
        ================================================== -->
        <link href="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/responsive.css">
        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/color/blue.css" id="colors">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/settings.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery.bxslider.css" media="screen">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery.fancybox.css" media="all" />
        <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Poppins:400,300,500,600'>
        <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700,600italic,400italic'>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/fonts/fontello.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/fonts/gizmo.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/fonts/picons.css">
        <!-- Fonts -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/font/font-awesome.min.css">
        <link href="<?php echo base_url();?>assets/css/animate.min.css" rel="stylesheet">
        <!--<link href="css/prettyPhoto.css" rel="stylesheet">-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/switcher/switcher.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        

    <!--[if lt IE 9]>
	<script src="../../../html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="<?php echo base_url();?>assets/js/modernizr.custom.js"></script>
</head>
