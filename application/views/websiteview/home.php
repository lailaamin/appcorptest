
<body>
    <div class="header-nav basic set yamm nobg">

        <div class="container">
            <div class="basic-wrapper">
                <a class="btn mobile-menu right" data-toggle="collapse" data-target=".navbar-collapse">
                    <i class='icon-menu-1'></i>
                </a>
                <a class="logo-white" href="index.php">
                    <img src="assets/images/appcorp_logo.png" />
                </a>
                <a class="logo-dark" href="index.php">
                    <img src="assets/images/appcorp_logo.png" />
                </a>
            </div>

            <div class="collapse navbar-collapse right">
                <!-- navigation -->
                <ul class="nav navbar-nav">
                    <li>
                        <a onclick="closeNav();" href="#xhome">Home</a>
                        <li>
                            <a onclick="closeNav();" href="#xabout_us">About Us</a>
                        </li>
                        <li>
                            <a onclick="closeNav();" href="#xproducts">Our Services</a>
                        </li>
                        <li>
                            <a onclick="closeNav();" href="#xportfolio">Partners</a>
                        </li>

                    </li>
                </ul>
                <!-- /navigation -->
            </div>
        </div>
    </div>
    <!-- / header -->

    <!-- content wrapper -->
    <div class="content-wrap">

        <div class="content" id="xhome">
            <!-- revolution slider -->
            <div class="banner-container revolution no-tmrg" >
                <div class="banner-full">
                    <ul>


                        <li data-transition="fade" style="z-index: 23;">
                            <img style="z-index: 19;" src="assets/images/img/bg-02.jpg" class="img-responsive" alt="" />


                            <div class="caption sfr lower" data-x="10" data-y="340" data-speed="1000" data-start="800" data-easing="Sine.easeOut" style="z-index: 23;"><p class="slide-title">Agile</p></div>
                            <div class="caption sfb upper1 hidden-xs hidden-sm" data-x="10" data-y="400" data-speed="1000" data-start="1200" data-easing="Sine.easeOut" style="z-index: 23;">
                                <p class="slide-body">Business solutions, you dream it. We make it come to life</p></div> 


                            </li>

                            <li data-transition="fade" style="z-index: 23;">
                                <img style="z-index: 19;" src="assets/images/img/bg-02.jpg" class="img-responsive" alt="" />
                                
                                
                                <div class="caption sfr lower" data-x="10" data-y="340" data-speed="1000" data-start="800" data-easing="Sine.easeOut" style="z-index: 23;"><p class="slide-title">Proactive</p></div>
                                <div class="caption sfr upper1 hidden-xs hidden-sm" data-x="10" data-y="400" data-speed="1000" data-start="1200" data-easing="Sine.easeOut" style="z-index: 23;">
                                    <p class="slide-body">Team Members who help create our world <br>of services and products</p></div>
                                </li> 

                                <li data-transition="fade">
                                    <img style="z-index: 19;" src="assets/images/img/bg-02.jpg" class="img-responsive" alt="" />



                                    <div class="caption sfr lower" data-x="10" data-y="340" data-speed="1000" data-start="800" data-easing="Sine.easeOut" style="z-index: 23;"><p class="slide-title">Passionate</p></div>
                                    <div class="caption sfr upper1 hidden-xs hidden-sm" data-x="10" data-y="400" data-speed="1000" data-start="1200" data-easing="Sine.easeOut" style="z-index: 23;">
                                        <p class="slide-body">About what we do, <br/>no matter how small it may seem to the world outside of our company</p></div>


                                    </li>

                                    <li data-transition="fade">
                                        <img style="z-index: 19;" src="assets/images/img/bg-02.jpg" class="img-responsive" alt="" />

                                        <div class="caption sfr lower" data-x="10" data-y="340" data-speed="1000" data-start="800" data-easing="Sine.easeOut" style="z-index: 23;"><p class="slide-title">Competitive</p></div>
                                        <div class="caption sfr upper1 hidden-xs hidden-sm" data-x="10" data-y="400" data-speed="1000" data-start="1200" data-easing="Sine.easeOut" style="z-index: 23;">
                                            <p class="slide-body">With the surrounding market</p></div>
                                        </li>

                                        <li data-transition="fade">
                                            <img style="z-index: 19;" src="assets/images/img/bg-02.jpg" class="img-responsive" alt="" />
                                            


                                            <div class="caption sfr lower" data-x="10" data-y="340" data-speed="1000" data-start="800" data-easing="Sine.easeOut" style="z-index: 23;"><p class="slide-title">Organized</p></div>
                                            <div class="caption sfr upper1 hidden-xs hidden-sm" data-x="10" data-y="400" data-speed="1000" data-start="1200" data-easing="Sine.easeOut" style="z-index: 23;">
                                                <p class="slide-body">Team members dedicated to delivering their best</p></div>

                                            </li>
                                            
                                            <li data-transition="fade">
                                                <img style="z-index: 19;" src="assets/images/img/bg-02.jpg" class="img-responsive" alt="" />
                                                
                                                <div class="caption sfr lower" data-x="10" data-y="340" data-speed="1000" data-start="800" data-easing="Sine.easeOut" style="z-index: 23;"><p class="slide-title">Responsive</p></div>
                                                <div class="caption sfr upper1 hidden-xs hidden-sm" data-x="10" data-y="400" data-speed="1000" data-start="1200" data-easing="Sine.easeOut" style="z-index: 23;">
                                                    <p class="slide-body">Team to make your dream a reality</p></div>
                                                </li>

                                                <li data-transition="fade">
                                                    <img style="z-index: 19;" src="assets/images/img/bg-02.jpg" class="img-responsive" alt="" />

                                                    <div class="caption sfr lower" data-x="10" data-y="340" data-speed="1000" data-start="800" data-easing="Sine.easeOut" style="z-index: 23;"><p class="slide-title">Performance</p></div>
                                                    <div class="caption sfr upper1 hidden-xs hidden-sm" data-x="10" data-y="400" data-speed="1000" data-start="1200" data-easing="Sine.easeOut" style="z-index: 23;">
                                                        <p class="slide-body">You will find, in no other organization</p></div>
                                                    </li>


                                                </ul>
                                            </div>
                                            <!-- /.banner -->
                                        </div>
                                    </div>
                                </div>
    <!-- Animated Intro
        ================================================== -->
        <div id="large-header" class="large-header hidden-md hidden-xs hidden-sm">
            <canvas id="demo-canvas"></canvas>
        </div>
        <!-- content wrapper -->
        <div class="content-wrap">

            <div class="content">
                <!-- -->
                <!-- banner section -->
                <div class="bg-gray" id="xabout_us">

                    <div class="container pad-container">
                        <div class="row t-mgr40 mob-mrg">
                            <div class="col-md-1" style="margin-bottom: 0px;"></div>
                            <div class="col-md-10 b-pad80">
                                <h2 class="heading t-pad80 wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="500ms" style="padding-top: 50px; padding-bottom: 20px;">
                                    <span>Who</span> We Are?</h2>
                                    <p style="font-family: 'Source Sans Pro', sans-serif; color: #2c2c2c" class="text-center l-pad60 r-pad60 wow fadeInDown animated" data-wow-duration="700ms"
                                    data-wow-delay="700ms">Our company is based in Cairo, founded by a team of four experts with 
                                    more than 15 years of experience in the fields of marketing, telecommunications, business 
                                    development and technology. Appcorp is a VAS Licensed company under the rules and regulations 
                                    of National Telecommunication Regulatory Authority (NTRA). Our core competence is the development
                                    and management of content, VAS solutions and services. We are dedicated to supporting mobile 
                                    operators in driving up VAS revenues, by providing them with the most innovative offerings in 
                                the industry.</p>
                            </div>
                            <div class="col-md-1" style="margin-bottom: 0px;"></div>
                        </div>
                    </div>
                </div>
                <!--/ banner section -->

                <!-- intro section -->
                <div class="bg-white t-pad60 b-pad80">
                    <div class="container">

                        <div class="row t-mgr30">
                            <div class="col-md-10 col-xs-offset-1">
                                <h2 class="heading wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="500ms">
                                    <span>What</span> we do?</h2>
                            <!-- <p class="h-sub l-pad80 r-pad80">We deliver a truly premium experience to web professionals. We believe in a diverse range of personel to bring creative skills, out of the box thoughts, and ideas to the table.
                            </p> -->
                        </div>
                    </div>

                    <div class="row t-mgr50">

                        <div class="col-md-3 text-center">
                            <div class="services-box-3">
                                <i class="icon-picons-diamond wow fadeInDown animated" data-wow-duration="700ms" data-wow-delay="700ms">
                                    <i class="circle-border"></i>
                                </i>
                                <div class="content wow fadeInUp animated" data-wow-duration="700ms" data-wow-delay="700ms">
                                    <h3>Proactivity</h3>
                                    <p style="color: #2c2c2c">We treat the root causes of the issues before they become an obstacle..</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 text-center">
                            <div class="services-box-3">
                                <i class="icon-picons-umbrella wow fadeInDown animated" data-wow-duration="900ms" data-wow-delay="900ms">
                                    <i class="circle-border"></i>
                                </i>
                                <div class="content wow fadeInUp animated" data-wow-duration="900ms" data-wow-delay="900ms">
                                    <h3>Flexibility</h3>
                                    <p style="color: #2c2c2c">We have the capability to turn challenges into chances and opportunities for growth.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 text-center">
                            <div class="services-box-3">
                                <i class="icon-picons-plane wow fadeInDown animated" data-wow-duration="1100ms" data-wow-delay="1100ms">
                                    <i class="circle-border"></i>
                                </i>
                                <div class="content wow fadeInUp animated" data-wow-duration="1100ms" data-wow-delay="1100ms">
                                    <h3>Support</h3>
                                    <p style="color: #2c2c2c">We are available around the clock, to help make your investment run smoothly.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 text-center">
                            <div class="services-box-3">
                                <i class="icon-picons-support-2 wow fadeInDown animated" data-wow-duration="1300ms" data-wow-delay="1300ms">
                                    <i class="circle-border"></i>
                                </i>
                                <div class="content wow fadeInUp animated" data-wow-duration="1300ms" data-wow-delay="1300ms">
                                    <h3>Creating value</h3>
                                    <p style="color: #2c2c2c">We create value in each service or product we put on the market.</p>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
            </div>
            
            <!-- intro section -->
            <div class="parallax bg-gray" id="xproducts">
                <div class="container pad-container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="heading t-pad80 wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="500ms" style="padding-top: 20px; padding-bottom: 20px;"><span>Our</span> Services</h2>
                        </div>
                    </div>
                    <br/><br/><br/>
                    
                    <div class="row">
                        <div class="col-md-5 wow fadeInLeft animated" data-wow-duration="700ms" data-wow-delay="700ms">
                            <h3 class="heading" style="color: #3c3c3c">Rakam1
                                <br> Etisalat Sport Platform</h3>
                                <p style="color: #2c2c2c"> 
                                    Keeping up with all the new in the world of sports and football Etisalat Misr has recently
                                    invaded the scene with a unique appearance launching a full-fledged sports platform
                                    “Etisalat Sports” to be the big umbrella for two main services; “Rakam1 & El Mal3ab”.
                                </p>
                                <p style="color: #2c2c2c">
                                    1- Rakam 1: A portal and SMS subscription service to keep the users updated with all 
                                    the local football news and media. Rakam1 launching campaign was featuring the Egyptian 
                                    goalkeeper "Essam El Hadary" with an insightful glimpse on his professional career, and 
                                    early life through videos and material that users can watch exclusively on Rakam 1 Portal.
                                </p>
                                <p style="color: #2c2c2c">
                                    2- El Mal3ab: SMS based subscription service that provides the user with all the latest 
                                    international football updates; games schedules, latest news,…etc. 
                                </p>
                            </div>
                            
                            <div class="col-md-6 col-xs-offset-1 t-mgr50 wow fadeInUp animated" data-wow-duration="1300ms" data-wow-delay="1300ms">
                                <p>
                                    <img class="img-center" src="assets/images/num1logo.png" alt="" >
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="parallax bg-white" id="xproducts">
                    <div class="container pad-container">
                        
                        <div class="row">
                            <div class="col-md-6 t-mgr50 wow fadeInUp animated" data-wow-duration="1300ms" data-wow-delay="1300ms">
                                <p>
                                    <img class="img-responsive" src="assets/images/Stad-vodafon-logo.png" style="max-width: 80%" alt="">
                                </p>
                            </div>
                            <div class="col-md-5 b-mgr40 wow fadeInLeft animated" data-wow-duration="700ms" data-wow-delay="700ms">
                                <h3 class="heading" style="color: #3c3c3c">Stad Vodafone
                                    <br> Sport Portal</h3>
                                    <p style="color: #2c2c2c">
                                        Stad Vodafone is the first mobile sports portal in Egypt, and one of the pioneers in the Middle East.  
                                        The portal has the official digital rights of the Egyptian Premier League (EPL), the Egyptian Cup & the 
                                        home games of the national Egyptian team in the World Cup Qualifications as well as friendly games. With
                                        a unique advantage over any other sports service in the region; Stad Vodafone provides Live streaming of
                                        all Egyptian Premier League (EPL), the Egyptian Cup and the home games of the national Egyptian team in 
                                        the World Cup Qualifications as well as friendly games. Stad Vodafone also shows a variety of information 
                                        concerning Egyptian football and championships. It highlights Tables, fixtures and results of different 
                                        matches and championships as well as updated posts of football news and headlines. In addition, Stad Vodafone 
                                        gives a short background about each Egyptian player and his portfolio; including how many goals the player 
                                        scored and how many yellow or red cards did the player receive. It will also give the opportunity to the 
                                        subscribers to interact on the portal through voting and predicting the results of the match.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="parallax bg-gray" id="xproducts">
                        <div class="container pad-container">
                            <div class="row">
                                <div class="col-md-5 wow fadeInLeft animated" data-wow-duration="700ms" data-wow-delay="700ms" style="margin-top: 10%">
                                    <h3 class="heading" style="color: #3c3c3c">Vodafone TV</h3>
                                    <p style="color: #2c2c2c">
                                        Video On Demand (VOD) portal which allows the subscriber to watch his/her favorite Ramadan series and programs
                                        exclusive without any commercial breaks whenever he/she wants.
                                    </p>
                                </div>
                                
                                <div class="col-md-6 col-xs-offset-1 t-mgr50 wow fadeInUp animated" data-wow-duration="1300ms" data-wow-delay="1300ms">
                                    <p>
                                        <img class="img-center" src="assets/images/vodaTV.png" alt="">
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="parallax bg-white" id="xproducts">
                        <div class="container pad-container">
                            <div class="row">
                                <div class="col-md-12 wow fadeInLeft animated" data-wow-duration="700ms" data-wow-delay="700ms">
                                    <h3 class="heading" style="color: #3c3c3c">More Services:</h3>
                                    <p style="color: #2c2c2c">
                                        We provide more than 20 service in sports and entertainment for operators both locally and across the region:
                                    </p>
                                    <p >
                                     <ol style="list-style: disc;">
                                         <li style="color: #2c2c2c">Etisalat & vodafone in Egypt</li>
                                         <li style="color: #2c2c2c">Mobily & Zain in Saudi Arabia</li>
                                         <li style="color: #2c2c2c">DU in UAE</li>
                                         <li style="color: #2c2c2c">Ooredoo in Qatar</li>
                                     </ol>                                              
                                 </p>
                             </div>
                         </div>
                     </div>
                 </div>
                 
             </div>
         </div>
         
         
         


         <div class="content-wrap" id="xportfolio">

            <div class="content">
                <div class="bg-gray">
                    <div class="container pad-container" style="padding-bottom: 20px">
                        <div class="owl-clients carousel">
                            <div class="item">
                                <img src="assets/images/content/partners/image3.png" alt="">
                            </div>
                            <div class="item">
                                <img src="assets/images/content/partners/image2.png" alt="">
                            </div>
                            <div class="item">
                                <img src="assets/images/content/partners/image1.png" alt="">
                            </div>

                            <div class="item">
                                <img src="assets/images/content/partners/05.png" alt="">
                            </div>
                            <div class="item">
                                <img src="assets/images/content/partners/11.png" alt="">
                            </div>
                            <div class="item">
                                <img src="assets/images/content/partners/07.png" alt="">
                            </div>
                            <div class="item">
                                <img src="assets/images/content/partners/Du_logo.png" alt="">
                            </div>
                            <div class="item">
                                <img src="assets/images/content/partners/04.png" alt="">
                            </div>
                            <div class="item">
                                <img src="assets/images/content/partners/03.png" alt="">
                            </div>
                            <div class="item">
                                <img src="assets/images/content/partners/09.png" alt="">
                            </div>
                            <div class="item">
                                <img src="assets/images/content/partners/08.png" alt="">
                            </div>
                            <div class="item">
                                <img src="assets/images/content/partners/06.png" alt="">
                            </div>
                            <div class="item">
                                <img src="assets/images/content/partners/10.png" alt="">
                            </div>
                            <div class="item">
                                <img src="assets/images/content/partners/001.png" alt="">
                            </div>
                            <div class="item">
                                <img src="assets/images/content/partners/12.png" alt="">
                            </div>
                            <div class="item">
                                <img src="assets/images/content/partners/13.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ client section -->
                <!-- footer -->
                
            </body>
