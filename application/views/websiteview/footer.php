            </footer>
            <!--/ footer -->
            <div style="background: #171717">
                <div class="container t-pad20 b-pad10">
                    <div class="col-md-12 t-pad10 copyright text-center">
                        <p class="bold">© 2018 Appcorp. All rights reserved.</p>
                    </div>
                </div>
            </div>

        </div>

        <!--/content wrapper -->
        <a id="scroll-top">
            <i class="fa fa-angle-up"></i>
            <span>Top</span>
        </a>


<!-- JS Files
    ================================================== -->
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script type="text/javascript">
        $(window).load(function () {
            $(".loading-overlay").fadeOut(1000, function () {
                $(".indexpage").css("overflow", "auto");
                $(".loading-overlay .spinner").fadeOut(1000);
                $(this).remove();
            });
        });
        var scrollButton = $("#scroll-top");
        $(window).scroll(function () {
            $(this).scrollTop() >= 500 ? scrollButton.show(800) : scrollButton.hide(800);
        });
        scrollButton.click(function () {
            $("html,body").animate({ scrollTop: 0 }, 800);
        });

        $(function () {
            $('a[href*="#x"]:not([href="#x"])').click(function () {
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        $('html, body').animate({
                            scrollTop: target.offset().top
                        }, 1000);
                        return false;
                    }
                }
            });
        });
    </script>
    <script src="<?php echo base_url();?>assets/js/jquery.themepunch.plugins.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.themepunch.revolution.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.inview.js"></script>
    <script src="<?php echo base_url();?>assets/js/isotope.pkgd.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/index.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.fancybox.pack.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.easytabs.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.sticky.js"></script>
    <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.sticky.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.zflickrfeed.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/twitter-bootstrap-hover-dropdown.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.easy-pie-chart.js"></script>
    <script src="<?php echo base_url();?>assets/js/classie.js"></script>
    <script src="<?php echo base_url();?>assets/js/search.js"></script>
    <script src="<?php echo base_url();?>assets/js/gizmo.js"></script>
    <script src="<?php echo base_url();?>assets/js/scripts.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.prettyPhoto.js"></script>
    <script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
    <script type="text/javascript">
    //Initiat WOW JS
    new WOW().init();
    function closeNav(){
        $(".navbar-collapse").removeClass("in");
        $(".navbar-collapse").removeClass("show");
    }
</script>
    </body>
</html>