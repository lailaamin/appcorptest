<div class="content-wrap">

   <div class="content" id="xhome">
    <!-- revolution slider -->
       <div class="banner-container revolution no-tmrg" >
           <div class="banner-full">
                <ul>
                    <?php foreach ($result as $obj ) {?>
                    <li data-transition="fade" style="z-index: 23;">
                        <img style="z-index: 19;" src="<?php echo base_url();?>assets/images/img/bg-02.jpg" class="img-responsive" alt="" />
                        <div class="caption sfr lower" data-x="10" data-y="340" data-speed="1000" data-start="800" data-easing="Sine.easeOut" style="z-index: 23;"><p class="slide-title"><?php echo $obj->title ?></p></div>
                        <div class="caption sfb upper1 hidden-xs hidden-sm" data-x="10" data-y="400" data-speed="1000" data-start="1200" data-easing="Sine.easeOut" style="z-index: 23;">
                            <p class="slide-body"><?php echo $obj->subtitle; ?></p></div> 

                    </li>
                    <?php }?>
                </ul>
            </div>
            <!-- /.banner -->
        </div>
    </div>
</div>