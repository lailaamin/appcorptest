    <div class="col-sm-4 ">
        <div class="widget r-pad30 ftitle">
            <div class="clearfix"></div>
            <p class="normal">Social Networks </p>
            <div class="social-icons">
                <ul style="padding: 0 0 0 3px;">
                 <?php foreach ($result as $obj ) { 

                  if($obj->Facebook!=''){
                    ?>
                    <li>
                        <a href="<?php echo $obj->Facebook;?>" class="facebook-alt" target="_blank" title="Facebook" data-rel="tooltip" data-placement="top"><i class="icon-facebook"></i></a>
                    </li>
                    <?php } if($obj->Linkedin!= ''){ ?>
                    <li>
                        <a href="<?php echo $obj->Linkedin;?>" class="linkedin-alt" target="_blank" title="Linkedin" data-rel="tooltip" data-placement="top"><i class="icon-linkedin"></i></a>
                    </li>
                    <?php } if($obj->Twitter!= ''){ ?>                        
                    <li>
                       <a href="$<?php echo $obj->Twitter; ?>" class="twitter-alt" target="_blank" title="Twitter" data-rel="tooltip" data-placement="top"><i class="icon-twitter"></i></a>
                    </li>
                <?php } if($obj->Instagram!= ''){ ?>
                    <li>
                        <a href="<?php echo $obj->Instagram; ?>" class="instagram-alt" target="_blank" title="Instagram" data-rel="tooltip" data-placement="top"><i class="icon-instagram"></i></a>
                    </li>
                <?php }}?>
            </ul>
        </div>
      </div>
    </div>
</div>
</div>