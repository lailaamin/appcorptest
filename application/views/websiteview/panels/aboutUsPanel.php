  <!-- Animated Intro
    ================================================== -->
    <div id="large-header" class="large-header hidden-md hidden-xs hidden-sm">
        <canvas id="demo-canvas"></canvas>
    </div>
    <!-- content wrapper -->
    <div class="content-wrap">

        <div class="content">
            <!-- -->
            <!-- banner section -->
            <div class="bg-gray" id="xabout_us">

                <div class="container pad-container">
                    <div class="row t-mgr40 mob-mrg">
                        <div class="col-md-1" style="margin-bottom: 0px;"></div>
                        
                        <?php foreach ($result as $obj ) {?>
                          <div class="col-md-10 b-pad80">
                            <h2 class="heading t-pad80 wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="500ms" style="padding-top: 50px; padding-bottom: 20px;">
                                <?php echo $obj->title;?></h2>
                                <p style="font-family: 'Source Sans Pro', sans-serif; color: #2c2c2c" class="text-center l-pad60 r-pad60 wow fadeInDown animated" data-wow-duration="700ms"
                                data-wow-delay="700ms"><?php echo $obj->body;?></p>
                            </div>
                        <?php } ?>
                        <div class="col-md-1" style="margin-bottom: 0px;"></div>
                    </div>
                </div>
            </div>
            <!--/ banner section -->
