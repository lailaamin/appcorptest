 <!-- intro section -->
 <div class="parallax bg-gray" id="xproducts" style="height: 100pt;">
    <div class="container pad-container" >
        <div class="row">
            <div class="col-md-12">
                <h2 class="heading t-pad80 wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="500ms" style="padding-top: 20px; padding-bottom: 20px;"><span>Our</span> Services</h2>
            </div>
        </div>
        <br/><br/><br/>
    </div>
</div>
<?php $counter=0; foreach ($result as $obj) {
    $counter++;?>
<div class="<?php if($counter%2==1){ echo'parallax bg-gray';} else {echo'parallax bg-white';}?>" id="xproducts">
    <div class="container pad-container">                     
        <div class="row">
          <?php if($obj->align === '1' || $obj->logo==NULL ){?>
            <div class="col-md-5 wow fadeInLeft animated" data-wow-duration="700ms" data-wow-delay="700ms">
                <h3 class="heading" style="color: #3c3c3c"><?php echo $obj->title;?></h3>
                <p style="color: #2c2c2c"> <?php echo $obj->body;?></p>
            </div>

            <div class="col-md-6 col-xs-offset-1 t-mgr50 wow fadeInUp animated" data-wow-duration="1300ms" data-wow-delay="1300ms">
                <p>
                    <img class="img-center img-responsive" src="<?php echo base_url()?>upload/<?php echo $obj->logo; ?>" alt="" style="max-width: 80%" >
                </p>
            </div>
            <?php }else if($obj->align === '0') { ?>
           <div class="col-md-6 col-xs-offset-1 t-mgr50 wow fadeInUp animated" data-wow-duration="1300ms" data-wow-delay="1300ms">
               <p>
                  <img class="img-responsive" src="<?php echo base_url()?>upload/<?php echo $obj->logo; ?>" alt=""  style="max-width: 80%">
              </p>
           </div>

          <div class="col-md-5 wow fadeInLeft animated" data-wow-duration="700ms" data-wow-delay="700ms">
            <h3 class="heading" style="color: #3c3c3c"><?php echo $obj->title;?></h3>
            <p style="color: #2c2c2c"> <?php echo $obj->body;?></p>
          </div>
       <?php }?>
      </div><!-- /.row-->
   </div>
</div>
<?php }?>



