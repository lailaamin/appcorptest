
<!-- intro section -->
<div class="bg-white t-pad60 b-pad80">
    <div class="container">

        <div class="row t-mgr30">
            <div class="col-md-10 col-xs-offset-1">
                <h2 class="heading wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="500ms">
                    <span>What</span> we do?</h2>
            </div>
        </div>

        <div class="row t-mgr50">
            <?php foreach ($result as $obj) { ?>

                <div class="col-md-3 text-center">
                    <div class="services-box-3">
                        <i class="<?php echo $obj->name;?> wow fadeInDown animated" data-wow-duration="700ms" data-wow-delay="700ms">
                            <i class="circle-border"></i>
                        </i>
                        <div class="content wow fadeInUp animated" data-wow-duration="700ms" data-wow-delay="700ms">
                            <h3><?php echo $obj->title;?></h3>
                            <p style="color: #2c2c2c"><?php echo $obj->description;?></p>
                        </div>
                    </div>
                </div>

            <?php } ?>


        </div>

    </div>
</div>
