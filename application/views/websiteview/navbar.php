
<body>
    <div class="header-nav basic set yamm nobg">

        <div class="container">
            <div class="basic-wrapper">
                <a class="btn mobile-menu right" data-toggle="collapse" data-target=".navbar-collapse">
                    <i class='icon-menu-1'></i>
                </a>
                <a class="logo-white" href="index.php">
                    <img src="<?php echo base_url();?>assets/images/appcorp_logo.png" />
                </a>
                <a class="logo-dark" href="index.php">
                    <img src="<?php echo base_url();?>assets/images/appcorp_logo.png" />
                </a>
            </div>

            <div class="collapse navbar-collapse right">
                <!-- navigation -->
                <ul class="nav navbar-nav">
                    <li>
                        <a onclick="closeNav();" href="#xhome">Home</a>
                        <li>
                            <a onclick="closeNav();" href="#xabout_us">About Us</a>
                        </li>
                        <li>
                            <a onclick="closeNav();" href="#xproducts">Our Services</a>
                        </li>
                        <li>
                            <a onclick="closeNav();" href="#xportfolio">Partners</a>
                        </li>

                    </li>
                </ul>
                <!-- /navigation -->
            </div>
        </div>
    </div>
    <!-- / header -->