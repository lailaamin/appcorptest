<?php

//File appcorp/application/libraries/Template.php:

/**
 * Template class
 *
 * This class implements application views methods
 * for both admin panel and website.
 *
 * @package     appcorp/application
 * @subpackage  models
 * @since       23/9/2018
 * @author      Laila Amin <lamin@app-corp.com>
 * @copyright   Copyright (c) 2018, Appcorp.
 * @link        http://localhost/appcorp/
 */

class Template {

 /**
  * Template constructor
  *
  * use the get_instance() which is reference
  * to the controller’s instance and stores 
  * the reference in the {@link CI} property
  *
  * @access public
  * @see CI_Controller
  * @return void
  */

  public function __construct()
  {
     $this->CI =& get_instance();
  }
 
 /**
  * loadView method
  *
  * This method is load the admin panel pages
  * by load header, sidebar and footer in
  * each admin panel page.
  *
  * @access public
  * @param String $view of the html block code
  * @param String array $vars of the data send to the view
  * @return void
  */

  public function loadView($view, $vars = array())
  {
     $this->CI->load->view('adminView/header', $vars);
     $this->CI->load->view('adminView/sidebar');	
     $this->CI->load->view($view, $vars);
     $this->CI->load->view('adminView/footer');
  }
 
 /**
  * loadLoginView method
  *
  * This method is load the admin panel login page
  * by load header , footer and login form.
  *
  * @access public
  * @param String $view of the html block code
  * @param String array $vars of the data send to the view
  * @return void
  */

  public function loadLoginView($view, $vars = array())
  {
     $this->CI->load->view('adminView/header', $vars);
     $this->CI->load->view($view, $vars);
     $this->CI->load->view('adminView/footer');
  }

   /**
  * loadLoginView method
  *
  * This method is load the whole website view
  *
  * @access public
  * @param String array $homePageResults of the data send to the home Panel view
  * @param String array $aboutUsResults of the data send to the about Us Panel view
  * @param String array $ourJobResults of the data send to the what We Do Panel view
  * @param String array $ourServicesResults of the data send to the our Service Panel view
  * @param String array $partnerResults of the data send to the partners Panel view
  * @param String array $contactUsFooter of the data send to the contact Us section view
  * @param String array $socilaNetworksResults of the data send to the social Networks section view
  * @return void
  */
   
  public function loadWebsiteView($homePageResults = array(), $aboutUsResults = array(),$ourJobResults = array(), $ourServicesResults = array(), $partnerResults = array(), $contactUsResults =array(),$socilaNetworksResults=array())
  {
     $this->CI->load->view('websiteView/header');
     $this->CI->load->view('websiteView/navbar');
     $this->CI->load->view('websiteView/panels/homePanel',$homePageResults);
     $this->CI->load->view('websiteView/panels/aboutUsPanel',$aboutUsResults);
     $this->CI->load->view('websiteView/panels/whatWeDoPanel',$ourJobResults);
     $this->CI->load->view('websiteView/panels/ourServicePanel',$ourServicesResults);
     $this->CI->load->view('websiteView/panels/partnersPanel',$partnerResults);	    
     $this->CI->load->view('websiteView/panels/aboutUsFooter',$aboutUsResults);
     $this->CI->load->view('websiteView/panels/contactUsFooter',$contactUsResults);
     $this->CI->load->view('websiteView/panels/socialNetworksFooter',$socilaNetworksResults);  
     $this->CI->load->view('websiteView/footer');
  }
}
?>