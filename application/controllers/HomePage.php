<?php

//File appcorp/application/controllers/HomePage.php:

/**
 * HomePage class
 *
 * This class implements index, add, update, delete and 
 * changePosition methods for slide show of the home page section of
 * Appcorp Admin Panel.
 *
 * @package     appcorp/application
 * @subpackage  csontrollers
 * @since       25/9/2018
 * @author      Laila Amin <lamin@app-corp.com>
 * @copyright   Copyright (c) 2018, Appcorp.
 * @link        http://localhost/appcorp/
 * @property    AdminModel AdminModel
 * @property    Template Template
 */

class HomePage extends CI_Controller{

 /**
  * index method
  *
  * This method is direct admin user to the homePage form
  * to add new data for the slide show or to homePpage table.
  *
  * @access public
  * @return void
  */

  public function index()
  {
    $action=$this->input->post('submit');
    $title=$this->input->post('title');
    $subtitle=$this->input->post('subtitle');

    if($action=='add')
    {
      $this->template->loadView('adminview/forms/homePage');
    }

    else
    {
       $results['result']=$this->AdminModel->getPosition('homepage');
       $this->template->loadView('adminview/tables/homePageTable',$results);
    }
  }

 /**
  * add method
  *
  * This method is direct admin user to the homePage form
  * to add new data which is entered from admin as post 
  * data($title and $subtitle) and direct admin to the
  * homePage table when he/she need to cancel or check 
  * that new data added successfully.
  *
  * @access public
  * @return void
  */

  public function add()
  {
     $submit1=$this->input->post('submit1');
     $title=$this->input->post('title');
     $subtitle=$this->input->post('subtitle');

     if($submit1=='save')
     {
        if ($title!=NULL || $subtitle != NULL)
        {
           $postData = array('title'=>$title,'subtitle'=>$subtitle);
           $this->AdminModel->addData('homepage',$postData);
           $this->template->loadView('adminview/forms/homePage');
        }
     }
     else if($submit1=='cancel')
     {      
       $results['result']=$this->AdminModel->getPosition('homepage');
       $this->template->loadView('adminview/tables/homePageTable',$results);  
     }
     else 
     {
       $this->template->loadView('adminview/forms/homePage');
     }
  }
 
 /**
  * update method
  *
  * This method is direct admin user to the homePage form
  * to update data which is entered from admin as post 
  * data($title and $subtitle) by using homePage id
  * and direct admin to the homePage table when
  * he/she need to cancel or check that data
  * updated successfully.
  *
  * @access public
  * @param integer $id id of homePage data
  * @return void
  */

  public function update($id='')
  {
     $submit1=$this->input->post('submit1');
     $title=$this->input->post('title');
     $subtitle=$this->input->post('subtitle');

     if ($submit1=='save') 
     {
        if ($title!=NULL || $subtitle != NULL)
        {
           $postData = array('title'=>$title,'subtitle'=>$subtitle);
           $this->AdminModel->update('homepage',$this->uri->segment(3),$postData);
           $results['result']=$this->AdminModel->getDataByID($id,'homepage');
           $this->template->loadView('adminview/forms/homePage',$results);
        }
     }
    else if ($submit1=='cancel')
    {      
        $results['result']=$this->AdminModel->getPosition('homepage');
        $this->template->loadView('adminview/tables/homePageTable',$results);  
    }
    else
    { 
        $results['result']=$this->AdminModel->getDataByID($this->uri->segment(3),'homepage');
        $this->template->loadView('adminview/forms/homePage',$results);
    }
  }

 /**
  * delete method
  *
  * This method is delete homePage
  * data(company email and location)
  * by using homepage data id.
  *
  * @access public
  * @param integer $id id of homePage data
  * @return void
  */

  public function delete($id='')
  {
    $this->AdminModel->delete('homepage',$this->uri->segment(3));
    $results['result']=$this->AdminModel->getPosition('homepage');
    $this->template->loadView('adminview/tables/homePageTable',$results);  
  }

 /**
  * changePosition method
  *
  * This method is needed for dragging and drop 
  * of the changing order of the row of the
  * data in the homePage table. 
  *
  * @access public
  * @return void
  */

  public function changePosition()
  {
    $this->AdminModel->getPosition('homepage');
    $position = $this->input->post('position1');
    $this->AdminModel->updateOrder($position,'homepage');
  }

}
?>