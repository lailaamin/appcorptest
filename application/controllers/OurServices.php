<?php

//File appcorp/application/controllers/OurServices.php:

/**
 * OurServices class
 *
 * This class implements index, add, update, delete and 
 * changePosition methods for our services section of
 * Appcorp Admin Panel.
 *
 * @package     appcorp/application
 * @subpackage  csontrollers
 * @since       7/10/2018
 * @author      Laila Amin <lamin@app-corp.com>
 * @copyright   Copyright (c) 2018, Appcorp.
 * @link        http://localhost/appcorp/
 * @property    AdminModel AdminModel
 * @property    Template Template
 */

class OurServices extends CI_Controller{

 /**
  * index method
  *
  * This method is direct admin user to the ourService form
  * to add new data to ourServices table.
  *
  * @access public
  * @return void
  */

  public function index()
  {
      $submit=$this->input->post('submit');

      if($submit=='add')
      {
         $this->template->loadView('adminview/forms/ourServicesForm');
      }
      else if ($submit=='update') 
      {
         $this->template->loadView('adminview/forms/ourServicesForm');
      }    
      else
      {
         $results['result']=$this->AdminModel->getPosition('ourservices');
         $this->template->loadView('adminview/tables/ourServicesTable',$results); 
      }
  }

 /**
  * add method
  *
  * This method is direct admin user to the ourService form
  * to add new data which is entered from admin as post 
  * data($title, $body, $image and $align) and direct admin to the
  * ourServices table when he/she need to cancel or check 
  * that new data added successfully.
  *
  * @access public
  * @return void
  */

  public function add()
  {
      $submit1=$this->input->post('submit1');
      $title=$this->input->post('title');
      $body= $this->input->post('body');
      $align=$this->input->post('align');
      
      if ($title!=NULL || $body != NULL)
      {
         $config['upload_path'] = './upload/';
         $config['allowed_types'] = 'jpg|jpeg|png|gif';
         $this->load->library('upload', $config);
         $this->upload->do_upload('img');
         $image = $this->upload->data();       
        
         $postData = array('title'=>$title,'body'=>$body,'logo'=>$image["file_name"],'align'=>$align);        
         $this->AdminModel->addData('ourservices',$postData);
         echo '<img src="'.base_url().'upload/'.$image["file_name"].'" width="300" height="225" class="img-thumbnail" />';
      }
      else if ($submit1=='cancel')
      {      
         $results['result']=$this->AdminModel->getPosition('ourservices');
         $this->template->loadView('adminview/tables/ourServicesTable',$results);  
      }      
  }
 
 /**
  * update method
  *
  * This method is direct admin user to the ourService form
  * to update data which is entered from admin as post 
  * data($title, $description and $icon) by using ourService id
  * and direct admin to the ourServices table when
  * he/she need to cancel or check that data
  * updated successfully.
  *
  * @access public
  * @param integer $id id of ourService data
  * @return void
  */  

  public function update($id='')
  {
      $submit1=$this->input->post('submit1');
      $title=$this->input->post('title');
      $body=$this->input->post('body');
      $align=$this->input->post('align');

     if ($title!=NULL || $body != NULL || $align != NULL)
     {  
      
         if (!isset($_FILES["img"]["name"])|| $_FILES["img"]["name"] =="") 
         {
              $postData = array('title'=>$title,'body'=>$body,'align'=>$align);
              $this->AdminModel->update('ourservices',$this->uri->segment(3),$postData);
        }
        else
        {
           $config['upload_path'] = './upload/';
           $config['allowed_types'] = 'jpg|jpeg|png|gif';
           $this->load->library('upload', $config);
           $this->upload->do_upload('img');
           $image = $this->upload->data();

           $postData = array('title'=>$title,'body'=>$body,'logo'=>$image["file_name"],'align'=>$align);
           $this->AdminModel->update('ourservices',$this->uri->segment(3),$postData);
        }     
      }
           
       $results['result'] = $this->AdminModel->getDataByID($id,'ourservices');
       $results['logoId'] = $id;
       $this->template->loadView('adminview/forms/ourServicesForm',$results);      
  }

 /**
  * delete method
  *
  * This method is delete ourService
  * data($title, $description and $icon)
  * by using ourService data id.
  *
  * @access public
  * @param integer $id id of ourService data
  * @return void
  */

  public function delete($id='')
  {
      $results['result']=$this->AdminModel->getPosition('ourservices');
      $imageName=$this->AdminModel->getDataByID($id,'ourservices');
      $imgpath='./upload/'.$imageName->logo;
      unlink($imgpath);
      $this->AdminModel->delete('ourservices',$this->uri->segment(3));
      $this->template->loadView('adminview/tables/ourServicesTable',$results);    
  }
 
 /**
  * changePosition method
  *
  * This method is needed for dragging and drop 
  * of the changing order of the row of the
  * data in the ourServices table. 
  *
  * @access public
  * @return void
  */

  public function changePosition()
  {
     $this->AdminModel->getPosition('ourservices');
     $position = $this->input->post('position4');
     $this->AdminModel->updateOrder($position,'ourservices'); 
  }


}

?>