<?php

//File appcorp/application/controllers/OurJob.php:

/**
 * OurJob class
 *
 * This class implements index, add, update, delete and 
 * changePosition methods for our Job section of
 * Appcorp Admin Panel.
 *
 * @package     appcorp/application
 * @subpackage  csontrollers
 * @since       5/10/2018
 * @author      Laila Amin <lamin@app-corp.com>
 * @copyright   Copyright (c) 2018, Appcorp.
 * @link        http://localhost/appcorp/
 * @property    AdminModel AdminModel
 * @property    Template Template
 */

class OurJob extends CI_Controller{

 /**
  * index method
  *
  * This method is direct admin user to the ourJob form
  * to add new data to ourJob table.
  *
  * @access public
  * @return void
  */

  public function index()
  {
    $submit=$this->input->post('submit');

    if($submit=='add')
    {  
        $data['result']=$this->AdminModel->getData('icons');
        $this->template->loadView('adminview/forms/ourJobForm',$data);
    }
    else
    {
         $data['result']=$this->AdminModel->getIcons();               
         $this->template->loadView('adminview/tables/ourJobTable',$data);
    }
  }

 /**
  * add method
  *
  * This method is direct admin user to the ourJob form
  * to add new data which is entered from admin as post 
  * data($title, $description and $icon) and direct admin to the
  * ourJob table when he/she need to cancel or check 
  * that new data added successfully.
  *
  * @access public
  * @return void
  */

  public function add()
  {
    $submit1=$this->input->post('submit1');
    $title=$this->input->post('title');
    $description=$this->input->post('description');
    $icon=$this->input->post('icons');

    if($submit1=='save')
    {
        
       if ($title!=NULL || $description != NULL|| $icon!=NULL)
       {
            $postData = array('title'=>$title,'description'=>$description,'icon'=>$icon);
            $this->AdminModel->addData('our_job',$postData);
            $results['result']=$this->AdminModel->getData('icons');
            $this->template->loadView('adminview/forms/ourJobForm',$results);  
        }
    }
    else if ($submit1=='cancel')
    {      
        $results['result']=$this->AdminModel->getIcons();
        $this->template->loadView('adminview/tables/ourJobTable',$results);                
    }
    else 
    {
        $results['result']=$this->AdminModel->getData('icons');
        $this->template->loadView('adminview/forms/ourJobForm',$results);
    }
  }

 /**
  * update method
  *
  * This method is direct admin user to the ourJob form
  * to update data which is entered from admin as post 
  * data($title, $description and $icon) by using ourJob id
  * and direct admin to the ourJob table when
  * he/she need to cancel or check that data
  * updated successfully.
  *
  * @access public
  * @param integer $id id of ourJob data
  * @return void
  */

  public function update($id='')
  {
     $submit1=$this->input->post('submit1');
     $title=$this->input->post('title');
     $description=$this->input->post('description');
     $icon=$this->input->post('icons');

     if($submit1=='save')
     {

       if ($title!=NULL || $description != NULL|| $icon!=NULL)
       {
            $postData = array('title'=>$title,'description'=>$description,'icon'=>$icon);
            $this->AdminModel->update('our_job',$this->uri->segment(3),$postData);
            $results['result']=$this->AdminModel->getData('icons');
            $results['result1']=$this->AdminModel->getDataByID($id,'our_job');
            $this->template->loadView('adminview/forms/ourJobForm',$results);  
       }
      } 
      else if ($submit1=='cancel')
      {      
         $results['result']=$this->AdminModel->getIcons();
         $this->template->loadView('adminview/tables/ourJobTable',$results);          
      }
      else 
      {
         $results['result']=$this->AdminModel->getData('icons');
         $results['result1']=$this->AdminModel->getDataByID($id,'our_job');
         $this->template->loadView('adminview/forms/ourJobForm',$results);
      }
  }

 /**
  * delete method
  *
  * This method is delete ourJob
  * data(title, description and icons)
  * by using ourJob data id.
  *
  * @access public
  * @param integer $id id of ourJob data
  * @return void
  */

  public function delete($id='')
  {
      $this->AdminModel->delete('our_job',$this->uri->segment(3));
      $results['result']=$this->AdminModel->getIcons();
      $this->template->loadView('adminview/tables/ourJobTable',$results);  
  }

 /**
  * changePosition method
  *
  * This method is needed for dragging and drop 
  * of the changing order of the row of the
  * data in the ourJob table. 
  *
  * @access public
  * @return void
  */

  public function changePosition()
  {
     $this->AdminModel->getIcons();
     $position = $this->input->post('position3');
     $this->AdminModel->updateOrder($position,'our_job'); 
  }

}

?>