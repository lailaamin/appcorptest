<?php

//File appcorp/application/controllers/AboutUs.php:

/**
 * AboutUs class
 *
 * This class implements index method which is needed
 * to handle the update of the about us section of
 * appcorp admin panel.
 *
 * @package     appcorp/application
 * @subpackage  csontrollers
 * @since       23/9/2018
 * @author      Laila Amin <lamin@app-corp.com>
 * @copyright   Copyright (c) 2018, Appcorp.
 * @link        http://localhost/appcorp/
 * @property    AdminModel AdminModel
 * @property    Template Template
 */

class AboutUs extends CI_Controller{

 /**
  * index method
  *
  * This method is updating the About Us section of
  * Appcorp by using the post data($title and $body)
  * which is entered by user.
  *
  * @access public
  * @return void
  */

 public function index()
 {
    $submit1=$this->input->post('submit1');
    $title=$this->input->post('title');
    $body=$this->input->post('body');

    if($submit1=='save')
    {  
      if ($title!=NULL || $body != NULL)
      {
          $postData = array('title'=>$title,'body'=>$body);
          $this->AdminModel->update('aboutus', 1 ,$postData);

          $results['result']=$this->AdminModel->getData('aboutus');       
          $this->template->loadView('adminview/forms/aboutUsForm',$results);  
      }
    }
    else 
    {
       $results['result']=$this->AdminModel->getData('aboutus');       
       $this->template->loadView('adminview/forms/aboutUsForm',$results);
    }
  }
}
?>