<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		// $this->load->view('welcome_message');
		$this->load_view('admin_View/Tables/About_Us_Table');
	// $this->load->view('admin_View/header');
	// // $this->load->view('admin_View/navbar');
	// $this->load->view('admin_View/sidebar');	
	// $this->load->view('admin_View/About_Us_AddForm');
	// $this->load->view('admin_View/footer');
	}

	public function load_view($view, $vars = array()) {
	    $this->load->view('admin_View/header', $vars);
	   	$this->load->view('admin_View/sidebar');	
	    $this->load->view($view, $vars);
	    $this->load->view('admin_View/footer');
  }
}
