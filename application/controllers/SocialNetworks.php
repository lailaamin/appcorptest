<?php

//File appcorp/application/controllers/SocialNetworks.php:

/**
 * SocialNetworks class
 *
 * This class implements index method which is needed
 * to handle the update of the about us section of
 * appcorp admin panel.
 *
 * @package     appcorp/application
 * @subpackage  csontrollers
 * @since       25/9/2018
 * @author      Laila Amin <lamin@app-corp.com>
 * @copyright   Copyright (c) 2018, Appcorp.
 * @link        http://localhost/appcorp/
 * @property    AdminModel AdminModel
 * @property    Template Template
 */

class SocialNetworks extends CI_Controller{

 /**
  * index method
  *
  * This method is updating the social networks
  * section of Appcorp by using the post data
  * ($Facebook, $Linkedin, $Twitter and $Instagram)
  * which is entered by user.
  *
  * @access public
  * @return void
  */

  public function index()
  {
    $submit=$this->input->post('submit1');
    $Facebook=$this->input->post('Facebook');
    $Linkedin=$this->input->post('Linkedin');
    $Twitter=$this->input->post('Twitter');
    $Instagram=$this->input->post('Instagram');

    if($submit=='save')
    {
       $postData=array('Facebook'=>$Facebook,'Linkedin'=>$Linkedin, 'Twitter'=>$Twitter,'Instagram'=>$Instagram);
       $this->AdminModel->update('socialnetworks', 1 ,$postData);

       $results['result']=$this->AdminModel->getData('socialnetworks');               
       $this->template->loadView('adminview/forms/socialNetworkForm',$results);  
    }
    else
    {        
       $results['result']=$this->AdminModel->getData('socialnetworks');
       $this->template->loadView('adminview/forms/socialNetworkForm',$results);  
    }
  }
}

?>