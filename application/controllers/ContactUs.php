<?php 

//File appcorp/application/controllers/ContactUs.php:

/**
 * ContactUs class
 *
 * This class implements index, add, update, delete and 
 * changePosition methods for contact us section of
 * Appcorp Admin Panel.
 *
 * @package     appcorp/application
 * @subpackage  csontrollers
 * @since       23/9/2018
 * @author      Laila Amin <lamin@app-corp.com>
 * @copyright   Copyright (c) 2018, Appcorp.
 * @link        http://localhost/appcorp/
 * @property    AdminModel AdminModel
 * @property    Template Template
 */

class ContactUs extends CI_Controller{ 
	
 /**
  * index method
  *
  * This method is direct admin user to the contatact us form
  * to add new data or to contact us information table.
  *
  * @access public
  * @return void
  */

  public function index()
  {
      $submit=$this->input->post('submit');

      if($submit=='add')
      {
         $this->template->loadView('adminview/forms/contactUsForm');
      }
      else
      {
         $results['result']=$this->AdminModel->getPosition('contactus');
         $this->template->loadView('adminview/tables/contactUsTable',$results);  
      }
  }
  
 /**
  * add method
  *
  * This method is direct admin user to the contatact us form
  * to add new data which is entered from admin as post 
  * data($email and $location) and direct admin to the
  * contac us table when he/she need to cancel or check 
  * that new data added successfully.
  *
  * @access public
  * @return void
  */

  public function add()
  {
      $submit1=$this->input->post('submit1');
      $email=$this->input->post('email');
      $location=$this->input->post('location');

      if($submit1=='save')
      {
         if ($email!=NULL || $location != NULL)
         {
            $postData = array('Email'=>$email,'location'=>$location);
            $this->AdminModel->addData('contactus',$postData);
            $this->template->loadView('adminview/forms/contactUsForm');  
          }
      }
      else if ($submit1=='cancel')
      {    
        $results['result']=$this->AdminModel->getPosition('contactus');
        $this->template->loadView('adminview/tables/contactUsTable',$results);  
      }
      else 
      {
        $this->template->loadView('adminview/forms/contactUsForm');
      }
  }

 /**
  * update method
  *
  * This method is direct admin user to the contatact us form
  * to update data which is entered from admin as post 
  * data($email and $location) by using contact us id
  * and direct admin to the contact us table when
  * he/she need to cancel or check that data
  * updated successfully.
  *
  * @access public
  * @param integer $id id of contact us data
  * @return void
  */
  public function update($id='')
  {
      $submit1=$this->input->post('submit1');
      $email=$this->input->post('email');
      $location=$this->input->post('location');

      if ($submit1=='save') 
      {
        if ($email!=NULL || $location != NULL)
        {

          $postData = array('Email'=>$email,'location'=>$location);
          $this->AdminModel->update('contactus',$this->uri->segment(3),$postData);
          $results['result']=$this->AdminModel->getDataByID($id,'contactus');
          $this->template->loadView('adminview/forms/contactUsForm',$results);

        }
      }
      else if ($submit1=='cancel')
      {      
         $results['result']=$this->AdminModel->getPosition('contactus');
         $this->template->loadView('adminview/tables/contactUsTable',$results);   
      }
      else
      { 
         $results['result']=$this->AdminModel->getDataByID($this->uri->segment(3),'contactus');
         $this->template->loadView('adminview/forms/contactUsForm',$results);
      }
  }

 /**
  * delete method
  *
  * This method is delete contact us 
  * data(company email and location)
  * by using contact us id.
  *
  * @access public
  * @param integer $id id of contact us data
  * @return void
  */

  public function delete($id='')
  {
    $this->AdminModel->delete('contactus',$this->uri->segment(3));
    $results['result']=$this->AdminModel->getPosition('contactus');
    $this->template->loadView('adminview/tables/contactUsTable',$results);  

  } 
  
 /**
  * changePosition method
  *
  * This method is needed for dragging and drop 
  * of the changing order of the row of the
  * data in the contact us table. 
  *
  * @access public
  * @return void
  */

  public function changePosition()
  {
    $this->AdminModel->getPosition('contactus');
    $position = $this->input->post('position2');
    $this->AdminModel->updateOrder($position,'contactus');
  }


}

?>