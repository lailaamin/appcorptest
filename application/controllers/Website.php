<?php

//File appcorp/application/controllers/Website.php:

/**
 * Website class
 *
 * This class implements index method which is needed
 * to display all information of each section that 
 * are entered by the admin user from the other side
 * Appcorp Admin Panel.
 *
 * @package     appcorp/application
 * @subpackage  csontrollers
 * @since       2/10/2018
 * @author      Laila Amin <lamin@app-corp.com>
 * @copyright   Copyright (c) 2018, Appcorp.
 * @link        http://localhost/appcorp/
 * @property    AdminModel AdminModel
 * @property    Template Template
 */

class Website extends CI_Controller{

 /**
  * index method
  *
  * This method is containing arrays($homePageResults,
  * $aboutUsResults,$ourJobResults,$ourServicesResults,
  * $partnerResults,$contactUsResults,$socilaNetworksResults)
  * each array stored the data for its section.
  *
  * @access public
  * @return void
  */

  public function index()
  {
    
    $homePageResults['result']=$this->AdminModel->getPosition('homepage');
    $aboutUsResults['result']=$this->AdminModel->getData('aboutus');
    $ourJobResults['result']=$this->AdminModel->getIcons();
    $ourServicesResults['result']=$this->AdminModel->getPosition('ourservices');
    $partnerResults['result']=$this->AdminModel->getPosition('partner');
    $contactUsResults['result']=$this->AdminModel->getPosition('contactus');
    $socilaNetworksResults['result']=$this->AdminModel->getData('socialnetworks');

  	$this->template->loadWebsiteView($homePageResults,$aboutUsResults,$ourJobResults,$ourServicesResults,$partnerResults,$contactUsResults,$socilaNetworksResults);    
    
  }

}
?>