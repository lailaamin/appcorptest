<?php

//File appcorp/application/controllers/OurPartners.php:

/**
 * OurPartners class
 *
 * This class implements index, add, update, delete and 
 * changePosition methods for partners section of
 * Appcorp Admin Panel.
 *
 * @package     appcorp/application
 * @subpackage  csontrollers
 * @since       6/10/2018
 * @author      Laila Amin <lamin@app-corp.com>
 * @copyright   Copyright (c) 2018, Appcorp.
 * @link        http://localhost/appcorp/
 * @property    AdminModel AdminModel
 * @property    Template Template
 */

class OurPartners extends CI_Controller{

 /**
  * index method
  *
  * This method is direct admin user to the partner form
  * to add new data to partners table.
  *
  * @access public
  * @return void
  */

  public function index()
  {
     $submit=$this->input->post('submit');

     if($submit=='add')
     {
        $this->template->loadView('adminview/forms/partnersForm');
     }    
     else
     {
        $results['result']=$this->AdminModel->getPosition('partner');
        $this->template->loadView('adminview/tables/partnersTable',$results);  
     }
  }

 /**
  * add method
  *
  * This method is direct admin user to the partner form
  * to add new data which is entered from admin as post 
  * data($image) and direct admin to the partners table
  * when he/she need to cancel or check that new data
  * added successfully.
  *
  * @access public
  * @return void
  */

  public function add()
  {
     $submit1=$this->input->post('submit1');
 
     if(isset($_FILES["img"]["name"]))
     {
        $config['upload_path'] = './upload/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $this->load->library('upload', $config);
        if(!$this->upload->do_upload('img'))
        {
          echo $this->upload->display_errors();
        }
        else
        {
          $image = $this->upload->data();
          $postData = array('logo'=>$image["file_name"]);
          $this->AdminModel->addData('partner',$postData);
          echo '<img src="'.base_url().'upload/'.$image["file_name"].'" width="300" height="225" class="img-thumbnail" />';
        }
      }
      else if ($submit1=='cancel')
      {      
          $results['result']=$this->AdminModel->getPosition('partner');
          $this->template->loadView('adminview/tables/partnersTable',$results);         
      }
  }

 /**
  * update method
  *
  * This method is direct admin user to the partner form
  * to update data which is entered from admin as post 
  * data($image) by using partner image id and direct
  * admin to the partners table when he/she need
  * to cancel or check that data updated successfully.
  *
  * @access public
  * @param integer  $id id of partner image
  * @return void
  */
  
  public function update($id='')
  {
      if(isset($_FILES["img"]["name"]))
      {   
         $config['upload_path'] = './upload/';
         $config['allowed_types'] = 'jpg|jpeg|png|gif';
         $this->load->library('upload', $config);
         if(!$this->upload->do_upload('img'))
         {
            echo $this->upload->display_errors();
         }
         else
         {
            $image = $this->upload->data();
            $postData = array('logo'=>$image["file_name"]);
            $this->AdminModel->update('partner',$this->uri->segment(3),$postData);      
         }
      }

      $results['result'] = $this->AdminModel->getDataByID($id,'partner');
      $results['logoId'] = $id;
      $this->template->loadView('adminview/forms/partnersForm',$results);
  }

 /**
  * delete method
  *
  * This method is delete partner
  * data(image)by using partner
  * image id.
  *
  * @access public
  * @param integer $id id of partner image
  * @return void
  */

  public function delete($id='')
  {
     
      $results['result']=$this->AdminModel->getPosition('partner');
      $imageName=$this->AdminModel->getDataByID($id,'partner');
      $imgpath='./upload/'.$imageName->logo;
      unlink($imgpath);
      $this->AdminModel->delete('partner',$this->uri->segment(3));
      $this->template->loadView('adminview/tables/partnersTable',$results);  
   
  }

 /**
  * changePosition method
  *
  * This method is needed for dragging and drop 
  * of the changing order of the row of the
  * data in the partners table. 
  *
  * @access public
  * @return void
  */
 
  public function changePosition()
  {
      $this->AdminModel->getPosition('partner');
      $position = $this->input->post('position');
      $this->AdminModel->updateOrder($position,'partner');
  }

}

?>