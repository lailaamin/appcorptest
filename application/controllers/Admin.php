<?php

//File appcorp/application/controllers/Admin.php:

/**
 * Admin class
 *
 * This class implements index, login and logout methods
 * for admin user of Appcorp Admin panel these methods 
 * are needed to handle actual user authentication, 
 * sessions and cookies of user.
 *
 * @package     appcorp/application
 * @subpackage  csontrollers
 * @since       29/9/2018
 * @author      Laila Amin <lamin@app-corp.com>
 * @copyright   Copyright (c) 2018, Appcorp.
 * @link        http://localhost/appcorp/
 * @property    AdminModel AdminModel
 * @property    Template Template
 */

class Admin extends CI_Controller{

 /**
  * index method
  *
  * This method is direct admin user to the admin login page.
  *
  * @access public
  * @return void
  */

  public function index()
  {
    $this->template->loadLoginView('adminview/login');    
  }
 
 /**
  * login method
  *
  * This method check the login username and password which is 
  * entered by admin user as post variables whether if login 
  * success: login session will store(username and user id)
  * and if user select remeber me: cookie will be created,
  * else if login failed user still in the login page.
  *
  * @access public
  * @return void
  */
  

  public function login()
  {
      $login=$this->input->post('login');
      $username=$this->input->post('username');
      $password=$this->input->post('password');
      $userid=$this->AdminModel->loginUser($username,$password);
      $remember=$this->input->post('remember');

      if($login=='login')
      {
         if (!$userid)
         {
           $this->template->loadLoginView('adminview/login');
         }

         else if ($userid || $remember == 1)
         {
              $cookie = array(
                'name'   => 'remember',
                'value'  => '1',
                'expire' => '31536000',
                'path'   => '/'
              );
              $this->input->set_cookie($cookie);
              $user_data=array(

                'ID'=>$userid,
                'username'=>$username,
                'logged_in' => TRUE
              );

              $this->session->set_userdata($user_data);
              $results['result']=$this->AdminModel->getPosition('homepage');
              $this->template->loadView('adminview/tables/homePageTable',$results);      
          }
          else
          {
            delete_cookie("remember");
          }
      }
      else
      {
        $this->template->loadLoginView('adminview/login');
      }
  }

 /**
  * logout method
  *
  * This method make admin logout from system by
  * clear user session by using admin id then
  * move admin user to the login page.
  *
  * @access public
  * @return void
  */

  public function logout()
  {
     $this->session->unset_userdata('ID');
     $this->session->sess_destroy();
     $this->template->loadLoginView('adminview/login');
  }
}
?>