//upload image script
$(document).ready(function(){
	$('#upload_form').on('submit', function(e){
		e.preventDefault();
		if($('#image_file').val() == '')
		{
			alert("Please Select the image");
		}
		else
		{
			$.ajax({
				url:"<?php echo base_url(); ?>index.php/admin/partnersForm", 
				//base_url() = http://localhost/tutorial/codeigniter
				method:"POST",
				data:new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				success:function(data)
				{$('#uploaded_image').html(data);
					
				}
			})
		}
	})
})


$(document).ready(function(){
    $('#upload_form1').on('submit', function(e){
        e.preventDefault();
        if($('#image_file1').val() == '')
        {
            alert("Please Select the image");
        }
        else
        {
            $.ajax({
                url:"<?php echo base_url(); ?>index.php/admin/ourservicesForm", 
                //base_url() = http://localhost/tutorial/codeigniter
                method:"POST",
                data:new FormData(this),
                contentType: false,
                cache: false,
                processData:false,
                success:function(data)
                {$('#uploaded_image1').html(data);
                    
                }
            })
        }
    })
})
//drag and drop for partner table
   $(document).ready(function(){
    	$( "#row_position" ).sortable({
        delay: 150,
        stop: function() {
            var selectedData = new Array();
            $('#row_position > tr').each(function() {
                selectedData.push($(this).attr("id"));
            });
            updatePosition(selectedData);
        }
    })
})

    function updatePosition(data) {
        $.ajax({
            url:"<?php echo base_url(); ?>index.php/admin/dragPartnertable",
            type:'post',
            data:{position:data},
            success:function(){
                alert('your change successfully saved');
            }
        })
    }
//drag and drop for home panel table
   $(document).ready(function(){
    	$( "#row_position1" ).sortable({
        delay: 150,
        stop: function() {
            var selectedData = new Array();
            $('#row_position1 > tr').each(function() {
                selectedData.push($(this).attr("id"));
            });
            updatePosition1(selectedData);
        }
    })
})

    function updatePosition1(data) {
        $.ajax({
            url:"<?php echo base_url(); ?>index.php/admin/dragHometable",
            type:'post',
            data:{position1:data},
            success:function(){
                alert('your change successfully saved');
            }
        })
    }


// drag and drop for contact us table
   $(document).ready(function(){
        $( "#row_position2" ).sortable({
        delay: 150,
        stop: function() {
            var selectedData = new Array();
            $('#row_position2 > tr').each(function() {
                selectedData.push($(this).attr("id"));
            });
            updatePosition2(selectedData);
        }
    })
})

    function updatePosition2(data) {
        $.ajax({
            url:"<?php echo base_url(); ?>index.php/admin/dragContactustable",
            type:'post',
            data:{position2:data},
            success:function(){
                alert('your change successfully saved');
            }
        })
    }
// drag and drop for what we do table
   $(document).ready(function(){
        $( "#row_position3" ).sortable({
        delay: 150,
        stop: function() {
            var selectedData = new Array();
            $('#row_position3 > tr').each(function() {
                selectedData.push($(this).attr("id"));
            });
            updatePosition3(selectedData);
        }
    })
})

function updatePosition3(data) {
    $.ajax({
        url:"<?php echo base_url(); ?>index.php/admin/dragWhatwedotable",
        type:'post',
        data:{position3:data},
        success:function(){
            alert('your change successfully saved');
        }
    })
}

// drg and drop for our service table
$(document).ready(function(){
        $( "#row_position4" ).sortable({
        delay: 150,
        stop: function() {
            var selectedData = new Array();
            $('#row_position4 > tr').each(function() {
                selectedData.push($(this).attr("id"));
            });
            updatePosition4(selectedData);
        }
    })
})

function updatePosition4(data) {
    $.ajax({
        url:"<?php echo base_url(); ?>index.php/admin/dragOurServicestable",
        type:'post',
        data:{position4:data},
        success:function(){
            alert('your change successfully saved');
        }
    })
}